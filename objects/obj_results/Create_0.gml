/// @description Insert description here


// Inherit the parent event
event_inherited();

title = "Thank you for playing!"

ds_list_set(lines,0,"You met " + string(ds_list_size(global.player_data.names_in_order)) + " students!")
if(ds_list_size(global.player_data.names_in_order) >= 8)
{
	ds_list_set(lines,1,"Scroll with up/down or the mouse wheel to see the full list",);
}

var names = global.player_data.names_in_order

for(var n = 0; n < ds_list_size(names); n++)
{
	ds_list_set(lines, n+3, ds_list_find_value(names,n));
}

exit_button = instance_create_layer(bbox_right-256, bbox_bottom - 128,"menus",abstract_button)
exit_button.build_button_full(spr_button_med,"End Game",restart_game);

alarm[0] = 180 * global.ROOM_SPEED_STANDARD;