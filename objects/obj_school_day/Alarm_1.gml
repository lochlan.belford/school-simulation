/// @description Student spawn timer (classrooms)
// You can write your code in this editor

show_debug_message("SPAWNING A BATCH OF STUDENTS FROM CLASS")
//Spawn a student at each classroom, if there are students left there
for(var c = 0; c < ds_list_size(classroom_list); c++)
{
	
	var classroom = ds_list_find_value(classroom_list, c);
	with(classroom)
	{
		students_spawned += spawn_student();
	}
}

//If still students left, reset timer
if(students_spawned < ds_list_size(student_roster))
{
	alarm[1] = global.STD_SPAWN_RATE;
}