/// @description Dim Screen on pause

if(global.paused)
{
	draw_sprite(spr_screen_dimmer,0,camera_get_view_x(view_camera[0]),0);
}

