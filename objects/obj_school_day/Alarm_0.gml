/// @description Student spawn timer (initial)

//Spawn a student at each spawn point, if there are students left
for(var s = 0; s < ds_list_size(spawn_point_list); s++)
{
	var spawn_point = ds_list_find_value(spawn_point_list, s);
	if(students_spawned < ds_list_size(student_roster))
	{
		var student_info = ds_list_find_value(student_roster, students_spawned);
		student_info.locker_number = ds_list_find_value(locker_numbers, lockers_assigned);
		lockers_assigned += 1;
		var student = instance_create_layer(spawn_point.x, spawn_point.y, "students", obj_npc_student)
		//finish initializing student
		with(student)
		{
			init_from_info(student_info);
			spawn_from_outside();
		}
		students_spawned += 1;
	}
}

//If still students left, reset timer
if(students_spawned < ds_list_size(student_roster))
{
	alarm[0] = global.STD_SPAWN_RATE;
}

