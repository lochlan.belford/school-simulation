/// @description Defines begin_day(), other functions
/*
This object contains the logic for running a single day of the school simualtion for The New Student
	On create:
	1. Receives the roster of students
	2. Receives the existing relationship information (if any)
	3. Receives all player character data
		-Always includes appearance and interests
		-If past the first day, this includes the relationship web
	4. Spawns the students
	5. Runs first class change
		- Schedules events
	6. Runs first class
		- Summarizes class for the player
	7. Repeats steps 5 & 6 three more times
	8. Runs the end of the day, students leave
	9. Passes data back to high-level game manager
*/

//All world objects should be between 0 and -9999, pause objects -10000 and beyond
//depth = -9999

student_roster = undefined;

game_state = GAME_STATES.START;

player_classroom = undefined;

card_stack_menu = instance_find(obj_card_stack,0);

time_to_end_day = false

//viewport switching functions
function zoom_in()
{
	view_visible[0] = true;
	view_visible[1] = false;
	instance_activate_object(card_stack_menu)
	instance_activate_object(card_stack_menu.top_card)
	instance_activate_object(card_stack_menu.menu_button)
	
}

function zoom_out()
{
	view_visible[0] = false;
	view_visible[1] = true;
	instance_deactivate_object(card_stack_menu.top_card)
	instance_deactivate_object(card_stack_menu)
	instance_deactivate_object(card_stack_menu.menu_button)
}

/*
	precondition: player and roster (above) MUST be filled by 
	the object creating this instance before calling begin_day()
*/

function begin_day()
{
	//build hallway boundaries
	for(var hall_x = global.HALL_LEFT; hall_x < global.HALL_RIGHT; hall_x += global.HALL_DOOR_WIDTH)
	{
		instance_create_layer(hall_x, global.HALL_TOP, "hallway", obj_hallway);
		instance_create_layer(hall_x, global.HALL_BOTTOM, "hallway", obj_hallway);
	}
	//build hallway
	classroom_list = ds_list_create();
	locker_list = ds_list_create();
	locker_numbers = ds_list_create();
	clock_list = ds_list_create();
	//Evenly space classrooms across hallway
	var classroom_spacing = (global.HALL_RIGHT - global.HALL_LEFT) / (global.HALL_NUM_CLASSROOMS + 1)
	var class_x = classroom_spacing;
	var next_locker_prefix = 1
	while(ds_list_size(classroom_list) < global.HALL_NUM_CLASSROOMS)
	{
		//create a classroom
		var classroom = instance_create_layer(class_x, global.HALL_TOP, "hallway", obj_classroom);
		ds_list_add(classroom_list, classroom);
		//create a locker block ahead of the classroom
		create_locker_block(class_x - 64, global.HALL_TOP,16,next_locker_prefix);
		//attempt adding a corkboard after classroom door
		instance_create_layer(class_x + 64, global.HALL_TOP,"hallway",obj_board);
		class_x += classroom_spacing;
		next_locker_prefix += 1;
	}
	//create one more locker block at the end of the hallway
	create_locker_block(global.HALL_RIGHT, global.HALL_TOP,16,next_locker_prefix);
	
	
	//create student spawn points, spaced evenly vertically just off screen left
	var spawn_spacing = (global.HALL_BOTTOM - global.HALL_TOP) / (global.HALL_NUM_SPAWNS + 1)
	var spawn_y = global.HALL_TOP + spawn_spacing;
	spawn_point_list = ds_list_create();
	while(ds_list_size(spawn_point_list) < global.HALL_NUM_SPAWNS)
	{
		var spawn = instance_create_layer(global.HALL_LEFT - 50, spawn_y, "hallway", obj_spawn_point);
		ds_list_add(spawn_point_list, spawn)
		spawn_y += spawn_spacing;
	}
	
	//shuffle the locker_numbers to give out on spawn
	ds_list_shuffle(locker_numbers);
	lockers_assigned = 0;
	
	//set first alarm to start spawning three at a time
	students_spawned = 0;
	alarm[0] = global.STD_SPAWN_RATE;
	
	
	//create player character
	player_character = instance_create_layer(global.HALL_LEFT + 200, global.HALL_TOP + 200, "students", obj_player_student);
	//give them a locker number
	global.player_data.locker_number = ds_list_find_value(locker_numbers, lockers_assigned);
	lockers_assigned += 1;
	with(player_character)
	{
		init_from_info(global.player_data);
		spawn_highlight();
	}
	//set viewport
	zoom_in();

	//Create "bell ring" alarm to force all students into classrooms after 2 minutes
	global.late_for_class = false;
	alarm[11] = global.CLASS_CHANGE_LENGTH * global.ROOM_SPEED_STANDARD;
	
	//Game state flags
	global.paused = false;
	global.interaction_in_progress = false;
	
	//enter first class change
	game_state = GAME_STATES.CLASS_CHANGE;
	
	//trigger menu at start of game
	obj_card_stack.show_menu();
}

function create_locker_block(end_x, locker_y, number_of_lockers, block_prefix)
{
	var locker_width = 64;
	var next_locker_x = end_x - (locker_width * number_of_lockers);
	var next_locker_number = block_prefix * 100 + 1;
	for(var lockers_made = 0; lockers_made < number_of_lockers; lockers_made += 1)
	{
		var next_locker = instance_create_layer(next_locker_x, locker_y, "lockers", obj_locker)
		next_locker.set_locker_number(next_locker_number);
		ds_list_add(locker_numbers,next_locker_number)
		ds_list_add(locker_numbers,next_locker_number + 1)
		ds_list_add(locker_list, next_locker);
		next_locker_x += locker_width;
		next_locker_number += 2;
	}
	
	//spawn a clock aligned with the last locker
	var next_clock = instance_create_layer(next_locker_x, 32,"hallway",obj_timer)
	ds_list_add(clock_list,next_clock);
}

/* rush_class_change()
	sets room_speed to x2 until all students have gone to class
*/
function rush_class_change()
{
	game_set_speed(global.ROOM_SPEED_FAST,gamespeed_fps)
	zoom_out();
}

/* finish_class_change()
	Resets game speed to default
	calls simulate_class()
*/
function finish_class_change(){
	zoom_in();
	show_debug_message("REACHED FINISH CLASS CHANGE")
	game_set_speed(global.ROOM_SPEED_STANDARD,gamespeed_fps);
	game_state = GAME_STATES.CLASS_IN_SESSION;
	simulate_class();
}

/* simulate_class()
*/
function simulate_class()
{
	show_debug_message("REACHED SIMULATE CLASS")
	for(var c = 0; c < ds_list_size(classroom_list); c++)
	{
		var classroom = ds_list_find_value(classroom_list, c);
		with(classroom)
		{
			start_class();
		}
		//Create a summary for the class the player is in
		if(classroom == player_classroom)
		{
			//meet between 3 and 12 students randomly
			var new_students = meet_students(classroom);
			var summary = instance_create_layer(x,y,"interactions",obj_class_summary);
			//if that failed for some reason, just move on immediately
			if(not instance_exists(summary))
			{
				begin_next_class_change();
			}
			//otherwise, hand control off to summary for now (it will call back to class change)
			else
			{
				summary.display_met_students(new_students);
			}
			//ds_list_destroy(new_students);
		}
		
	}
	//do notebook paper stuff for player's classroom, then
}

/*
*/
function begin_next_class_change()
{
	//if visited 4 classes, instead goto end of game
	if(global.player_data.classes_visited >= 4)
	{
		time_to_end_day = true;
	}
	else
	{
		show_debug_message("REACHED BEGIN CLASS CHANGE")
		//spawn player
		player_character = instance_create_layer(player_classroom.x, player_classroom.y, "students", obj_player_student);
		with(player_character)
		{
			init_from_info(global.player_data);
			spawn_highlight();
		}
		show_debug_message("PLAYER SPAWNED")
		//reset timers
		for(var clocks = 0; clocks < ds_list_size(clock_list); clocks++)
		{
			var next_clock = ds_list_find_value(clock_list,clocks)
			with(next_clock)
			{
				reset_timer();
			}
		}
		show_debug_message("CLOCK TIMERS RESET")
		//set student spawn timer
		students_spawned = 0;
		alarm[1] = global.STD_SPAWN_RATE;
	
		show_debug_message("SPAWN TIMERS RESET")
	
		//set viewport
		zoom_in()
		show_debug_message("VIEWPORTS RESET")

		//Create "bell ring" alarm to force all students into classrooms after 2 minutes
		global.late_for_class = false;
		alarm[11] = global.CLASS_CHANGE_LENGTH * global.ROOM_SPEED_STANDARD;
		show_debug_message("GLOBAL ALARM RESET")
	
		global.paused = false;
	
		//enter next class change
		game_state = GAME_STATES.CLASS_CHANGE;
	}
}

//Takes first 3-12 students from shuffled class list
function form_random_group(class)
{
	var group = ds_list_create();
	var num_students = irandom_range(3,12)
	if(num_students > ds_list_size(class.students))
	{
		num_students = ds_list_size(class.students);
	}
	//student roster has already been shuffled, so just take first num_students
	for(var s = 0; s < num_students; s++)
	{
		ds_list_add(group,ds_list_find_value(class.students,s))
	}
	return group
}

function meet_students(class)
{
	var students_in_group = form_random_group(class)
	var new_students = ds_list_create()
	for(var s = 0; s < ds_list_size(students_in_group); s++)
	{
		var next_student = ds_list_find_value(students_in_group,s);
		var is_new_student = meet_student(next_student);
		if(is_new_student)
		{
			ds_list_add(new_students,next_student)
		}
	}
	ds_list_destroy(students_in_group)
	return new_students;
}


function end_day()
{
	results_screen = instance_create_layer(camera_get_view_x(view_camera[0]) +global.PAPER_X,camera_get_view_y(view_camera[0]) +global.PAPER_Y,"interactions",obj_results);
	global.paused = true;
	global.interaction_in_progress = true;
}