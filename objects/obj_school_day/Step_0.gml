/// @description Pause increments
// You can write your code in this editor

if global.paused
{
	alarm[0] += 1;
	alarm[1] += 1;
	alarm[11] += 1;
}

if game_state = GAME_STATES.CLASS_CHANGE
{
	//If no students left (including player), class change is over
	if instance_number(abstract_student) <= 0
	{
		finish_class_change();
	}
	//If player is gone (in class) but other students remain, speed up
	else if instance_number(obj_player_student) <= 0
	{
		rush_class_change();
	}
}

if time_to_end_day
{
	time_to_end_day = false
	end_day();
}