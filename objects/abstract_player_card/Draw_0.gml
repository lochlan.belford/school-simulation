/// @description Draws card title, and six lines
// You can write your code in this editor
draw_self();
draw_set_font(fnt_paper);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
//Draw title
draw_text_color(x, y - line_height, title,c_black,c_black,c_black,c_black,1);
for(var c = 0; c < num_lines; c++)
{
	var next_line = ds_list_find_value(lines,(c+line_window_start));
	//Write line if it is not blank
	if(string_length(next_line) > 0)
	{
		//write number before left rule, if numbered
		if(show_line_numbers)
		{
			draw_text_color(x - 64, y + line_height * (c),string(c+1+line_window_start) + ".",c_black,c_black,c_black,c_black,1)
		}
		//write text
		draw_text_color(x + 32,y + line_height * (c),next_line,c_black,c_black,c_black,c_black,1);
	}
}