/// @description Create notecard data structure
// You can write your code in this editor

//Based on card sprite
line_height = 64;
num_lines = 6;
//To support more than six lines, create counter of current top line
line_window_start = 0;
//Title stored separately so it always displays above lines
title = ""
//Create a list of six strings to print on the six lines of the card
lines = ds_list_create()
//flag for drawing line numbers to left of lines
show_line_numbers = true;
for(var line = 0; line < num_lines; line++)
{
	ds_list_add(lines,"");
}

//Scrolls up if not already at top
function scroll_up()
{
	if(line_window_start > 0)
	{
		line_window_start -= 1;
	}
}

//Scrolls visible lines downward, if any are left
function scroll_down()
{
	if(line_window_start < ds_list_size(lines) - num_lines)
	line_window_start += 1;
}
