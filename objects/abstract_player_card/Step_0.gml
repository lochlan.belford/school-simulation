/// @description check for keyboard scrolling

if(keyboard_check_pressed(vk_up) or keyboard_check_pressed(ord("W")))
{
	scroll_up();
}
else if(keyboard_check_pressed(vk_down) or keyboard_check_pressed(ord("S")))
{
	scroll_down();
}