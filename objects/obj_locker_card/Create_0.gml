/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

title = "My Locker Number"
show_line_numbers = false;
ds_list_set(lines,1,string(global.player_data.locker_number));
if(obj_player_student.has_book)
{
	ds_list_set(lines,3,"You already got your textbook.");
}
else
{
	ds_list_set(lines,3,"You need to get your textbook");
	ds_list_set(lines,4,"from your locker before class.");
}