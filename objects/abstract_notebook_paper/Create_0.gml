/// @description Create notebook paper data structure

//Based on card sprite
line_height = 47;
num_lines = 11;
//To support more than eleven lines, create counter of current top line
line_window_start = 0;
//Title stored separately so it always displays above lines
title = ""
//Create a list of six strings to print on the six lines of the card
lines = ds_list_create()
//flag for drawing line numbers to left of lines
show_line_numbers = false;

//text can be centered or left-justified
centered = false;


for(var line = 0; line < num_lines; line++)
{
	ds_list_add(lines,"");
}

//Scrolls up if not already at top
function scroll_up()
{
	if(line_window_start > 0)
	{
		line_window_start -= 1;
	}
}

//Scrolls visible lines downward, if any are left
function scroll_down()
{
	if(line_window_start < ds_list_size(lines) - num_lines)
	line_window_start += 1;
}
