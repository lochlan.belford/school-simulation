/// @description Shared sprite modification code

// Both players and npcs use the same base sprite, and the same variables to transform it
draw_sprite_ext(sprite_index, sprite_choice, x, y, image_xscale, image_yscale, image_angle, blend_color, image_alpha);
draw_set_font(fnt_student);
draw_set_halign(fa_left);
draw_set_valign(fa_top);