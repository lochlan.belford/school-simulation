/// @description Abstract class for PC and NPCs

//student_info packages all fields together for easy transfer
my_info = undefined

//Initialize default name and appearance
student_id = "No ID";
name = "No Name";
sprite_index = spr_students_solid_line_large;
sprite_choice = irandom(2);
x_scale = 1;
y_scale = 1;
blend_color = c_white;

//initialize data variables
schedule = ds_list_create();
interests = ds_list_create();
subjects = ds_list_create();
relationships = ds_map_create();

//Initialize movement and action variables
classes_visited = 0;
talk_glyph = 0;
speak = false;
draw_bubble = false;
move_state = MOVEMENT_STATES.START;
debug_move_state_text = "START";
destination_x = 0; 
destination_y = 0;
destination_epsilon = 5;
destination_classroom = undefined;
locker_number = 101

//Initialize UI variables
selected = false;
am_paused = false;


/*  init_from_info(StudentInfo)
	takes in a StudentInfo struct and reads its fields,
	copying their values to the relevant instance variables for this instance
	[StudentInfo is defined in the main logic object's GameStart event]
*/
function init_from_info(student_info)
{
	my_info = student_info;
	student_id = my_info.id;
	name = my_info.name;
	sprite_choice = my_info.sprite_choice;
	//for scale, set the built-in scaling variables to alter bounding box as well
	image_xscale = my_info.xscale;
	image_yscale = my_info.yscale;
	blend_color = make_color_hsv(my_info.hue, my_info.sat, my_info.lum);
	ds_list_copy(interests, my_info.interests);
	ds_list_copy(subjects, my_info.subjects);
	ds_map_copy(relationships, my_info.relationships);
	ds_list_copy(schedule, my_info.schedule);
	classes_visited = my_info.classes_visited;
	locker_number = my_info.locker_number;
	known_to_player = my_info.known_to_player;
	determine_destination();
}

function determine_destination()
{
	//If you have classes left to visit, go to class
	if(classes_visited < ds_list_size(schedule))
	{
		//get next
		destination_number = ds_list_find_value(schedule, classes_visited)
		//find matching classroom, set as destination
		for(var i = 0; i < instance_number(obj_classroom); i++)
		{
			var classroom = instance_find(obj_classroom, i);
			if classroom.room_number == destination_number 
			{
				destination_x = classroom.x;
				destination_y = classroom.y;
				destination_classroom = classroom;
				break;
			}
		}
	}
	//TODO: other destinations, such as lunchroom and home
}

function enter_random_movement_state()
{
	move_state = irandom_range(1, MOVEMENT_STATES.length - 1);
	enter_movement_state(move_state);
}
function enter_movement_state(move_state)
{
	//set parameters for that state
	switch(move_state){
		case MOVEMENT_STATES.STOP:
			speed = 0;
			debug_move_state_text = "STOP";
			break;
		case MOVEMENT_STATES.WANDER:
			speed = 3;
			direction = random_range(0, 360);
			debug_move_state_text = "WANDER";
			break;
		case MOVEMENT_STATES.WALK:
			speed = 5;
			direction = random_range(0, 360);
			debug_move_state_text = "WALK";
			break;
		case MOVEMENT_STATES.RUN:
			speed = 9;
			direction = random_range(0, 360);
			debug_move_state_text = "RUN";
			break;
		case MOVEMENT_STATES.TO_CLASSROOM:
			speed = 3;
			direction = point_direction(x, y, destination_x, destination_y);
			debug_move_state_text = "TO_CLASS";
			break;
		case MOVEMENT_STATES.TO_SPOTLIGHT:
			debug_move_state_text = "TO_SPOT";
		default:
			speed = 1;
			direction = random_range(0, 360);
			break;
	}
}
