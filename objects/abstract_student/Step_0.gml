/// @description Bounds checking, wrapping

//Pause and Unpause code
if(global.paused)
{
	//Stop moving (only do this once)
	if(not am_paused)
	{
		am_paused = true;
		old_speed = speed;
		speed = 0;
	}
	//Cycle all alarms (do this every step)
	for(var a = 0; a < 12; a++)
	{
		alarm[a] += 1;
	}
}

if(am_paused and !global.paused)
{
	am_paused = false;
	speed = old_speed
}

//check for speech
if(speak)
{
	draw_bubble = true;
	alarm[2] = 1 * global.ROOM_SPEED_STANDARD;
	speak = false;
}

//wrap x movement
move_wrap(true, false, sprite_width);

//Check for collision with upper and lower bounds of hallway
if(y < global.HALL_TOP) //This is confusing because y increases downward
{
	//Choose downward direction
	direction = random_range(180, 360)
}
else if(y > global.HALL_BOTTOM)
{
	//choose upward direction
	direction = random_range(0, 180);
}

//testing z-index changing by height
depth = -y
