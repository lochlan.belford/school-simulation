/// @description Insert description here
// You can write your code in this editor
if(being_held)
{
	if(mouse_x < min_bound)
	{
		x = min_bound;
	}
	else if(mouse_x >= max_bound)
	{
		x = max_bound - 1;
	}
	else
	{
		x = mouse_x;
	}
	if(not mouse_check_button(mb_any))
	{
		being_held = false;
		image_index = state_dormant;
	}
}