/// @description Insert description here
// You can write your code in this editor

y_scale = height_slider.current_val;
x_scale = width_slider.current_val;
hue = hue_slider.current_val;
sat = sat_slider.current_val;
lum = lum_slider.current_val;
blend_color = make_color_hsv(hue, sat, lum);

name = keyboard_string;

//Check for button clicks
if(mouse_check_button_pressed(mb_any))
{
	show_debug_message("Left Pressed")
	if(mouse_y > switcher_top)
	{
		//In square section
		if(mouse_x >= square_left and mouse_x <= square_right)
		{
			shape_index = 0;
		}
		//In circle section
		if(mouse_x >= circle_left and mouse_x <= circle_right)
		{
			shape_index = 1;
		}
		//In triangle section
		if(mouse_x >= triangle_left and mouse_x <= triangle_right)
		{
			shape_index = 2;
		}
	}
}