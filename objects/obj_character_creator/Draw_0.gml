/// @description Insert description here
// You can write your code in this editor


draw_rectangle(x,y,width, height, true)

//Draw Title
draw_set_font(fnt_paper)
draw_set_halign(fa_left);
draw_set_valign(fa_bottom);
draw_text_color(100,48*2 + 5,"Create Your Student! ",c_black,c_black,c_black,c_black,1)
draw_text_color(100,48*4 + 5,"Type Name ",c_black,c_black,c_black,c_black,1)

draw_set_font(fnt_paper);

//Draw Name box
draw_text_color(100, 48*5 + 10, "Here:     " + name,c_black,c_black,c_black,c_black,1);

//draw_set_font(fnt_paper_med);

//Draw Sprite
draw_sprite_ext(student_sprite, shape_index, (column[0]+column[1]) / 2, row[3],x_scale*3,y_scale*3,0,blend_color,1);

//Draw Shape buttons
//Square button outline
draw_set_color(c_white)
if(shape_index = 0)
{
	draw_set_color(c_black)
}
draw_rectangle(square_left,switcher_top,square_right,switcher_bottom,false)
draw_rectangle_color(square_left,switcher_top,square_right,switcher_bottom,c_black,c_black,c_black,c_black,true)
//Square button symbol
draw_sprite(spr_std_rect,0,(square_right+square_left)/2,(switcher_bottom+switcher_top)/2)
//Circle Button outline
draw_set_color(c_white)
if(shape_index = 1)
{
	draw_set_color(c_black)
}
draw_rectangle(circle_left,switcher_top,circle_right,switcher_bottom,false)
draw_rectangle_color(circle_left,switcher_top,circle_right,switcher_bottom,c_black,c_black,c_black,c_black,true)
//Circle button symbol
draw_sprite(spr_std_circ,0,(circle_right+circle_left)/2,(switcher_bottom+switcher_top)/2)
//Tri Button outline
draw_set_color(c_white)
if(shape_index = 2)
{
	draw_set_color(c_black)
}
draw_rectangle(triangle_left,switcher_top,triangle_right,switcher_bottom,false)
draw_rectangle_color(triangle_left,switcher_top,triangle_right,switcher_bottom,c_black,c_black,c_black,c_black,true)
//Tri button symbol
draw_sprite(spr_std_tri,0,(triangle_right+triangle_left)/2,(switcher_bottom+switcher_top)/2)
draw_set_color(c_white)