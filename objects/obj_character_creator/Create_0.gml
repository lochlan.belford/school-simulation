/// @description Insert description here
// You can write your code in this editor

/*
The character creator is a UI panel that contains:
	* A display of the player character on the full left
	* Three buttons for selecting sprite shape in the upper center
	* A slider in the center center for height
	* A slider in the lower center for width
	* Sliders for Hue, Saturation, and Luminescence in the upper, middle, and lower right
*/

//Move to room origin regardless of where placed in room view
x = 0;
y = 0;
height = room_height;
width = room_width;
column[0] = x
column[1] = width/2

row[0] = y
row[1] = 1*height/5 
row[2] = 2*height / 5 
row[3] = 3*height/5
row[4]= 4*height/5
row[5] = height


//create name entry box
name = ""

//Display the sprite you are creating
x_scale = 1;
y_scale = 1;
blend_color = c_white;
student_sprite = spr_students_solid_line_large
shape_index = 0;

//TODO: create sprite switcher
switcher_top = row[4];
switcher_bottom = row[5];
square_left = column[0]
square_right = column[1]/3
circle_left = square_right + 1
circle_right = 2*column[1]/3
triangle_left = circle_right + 1
triangle_right = column[1]


//create appearance sliders
height_slider = instance_create_layer(column[1] + 50, (row[0] + row[1])/2, "sliders", obj_slider)
with(height_slider)
{
	depth = -50
	title = "Height";
	min_val = global.STD_MIN_YSCALE;
	max_val = global.STD_MAX_YSCALE;
	handle.x += 80;
}
width_slider = instance_create_layer(column[1] + 50, (row[1] + row[2])/2, "sliders", obj_slider)
with(width_slider)
{
	depth = -50
	title = "Width";
	min_val = global.STD_MIN_XSCALE;
	max_val = global.STD_MAX_XSCALE;
	handle.x += 200
}

//create color sliders
hue_slider = instance_create_layer(column[1] + 50,(row[2] + row[3])/2,"sliders", obj_slider)
with(hue_slider)
{
	title="Hue";
	min_val = global.STD_MIN_HUE;
	max_val = global.STD_MAX_HUE;
	handle.x += 400
	round_val = true;
}

sat_slider = instance_create_layer(column[1] + 50,(row[3] + row[4])/2,"sliders", obj_slider)
with(sat_slider)
{
	title="Saturation";
	min_val = global.STD_MIN_SAT;
	max_val = global.STD_MAX_SAT;
	handle.x += 400
	round_val = true;
}

lum_slider = instance_create_layer(column[1] + 50,(row[4] + row[5])/2,"sliders", obj_slider)
with(lum_slider)
{
	title="Luminosity";
	min_val = global.STD_MIN_LUM;
	max_val = global.STD_MAX_LUM
	handle.x += 400
	round_val = true;
}