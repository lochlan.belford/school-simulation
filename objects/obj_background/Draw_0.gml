/// @description Insert description here
// You can write your code in this editor
draw_rectangle_color(0,0,room_width,room_height,c_aqua,c_aqua,c_gray,c_gray,false)

//draw fps
if debug_mode and room == rm_new_student_hall{
	draw_set_font(fnt_debug);
	draw_text(32, 32, "FPS = " + string(fps));
	draw_text(32, 64, "Number of Students = " + string(instance_number(obj_npc_student)));
	draw_text(32, 96, "Random Seed = " + string(random_get_seed()));
}