/// @description Insert description here


// Inherit the parent event
event_inherited();



//Display subject and room number
var text = target.room_title + ", Room" + string(target.room_number);
title = text;


//buttons
function confirm()
{
	
	//pass my destination classroom to school_day
	obj_school_day.player_classroom = target;
	//record I have visited another class
	global.player_data.classes_visited += 1;
	//mark for destruction
	destroy_player = true;
	instance_destroy();
	
}
function cancel()
{

	instance_destroy();
}

//If correct classroom
var player_schedule = global.player_data.schedule;
var class_count = global.player_data.classes_visited;
if(target.room_number = ds_list_find_value(player_schedule,class_count))
{
	//check if you have your books
	if(player.has_book)
	{
		//ask if you want to enter the class
		ds_list_set(lines,1,"Enter Classroom?");

		//allow you to confirm or deny
		confirm_button = instance_create_layer(x + 128,y + line_height * 5,"interactions",abstract_button);
		confirm_button.build_button_full(spr_button_small,"Confirm",confirm)
		ds_list_add(buttons,confirm_button);

		cancel_button = instance_create_layer(x + 512,y + line_height * 5,"interactions",abstract_button);
		cancel_button.build_button_full(spr_button_small,"Cancel",cancel)
		ds_list_add(buttons,cancel_button);
	}
	else
	{
		//tell them to go get your book
		ds_list_set(lines,0,"You don't have your textbook!");
		ds_list_set(lines,1,"It's in your locker.");
		ds_list_set(lines,2,"(You can check your locker");
		ds_list_set(lines,3,"number in the menu.)");
		
		cancel_button = instance_create_layer(x + 512,y + line_height * 5,"interactions",abstract_button);
		cancel_button.build_button_full(spr_button_small,"Cancel",cancel)
		ds_list_add(buttons,cancel_button);
	}
}
//otherwise, say you've got the wrong room
else
{
	ds_list_set(lines,1,"This is not your next class!");
	ds_list_set(lines,2,"(You can check your schedule");
	ds_list_set(lines,3,"in the menu.)");
	
	
	cancel_button = instance_create_layer(x + 512,y + line_height * 5,"interactions",abstract_button);
	cancel_button.build_button_full(spr_button_small,"Cancel",cancel)
	ds_list_add(buttons,cancel_button);
	
}
