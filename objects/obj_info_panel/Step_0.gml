/// @description Insert description here
// You can write your code in this editor
if(mouse_check_button_pressed(mb_left))
{
	//show_debug_message("Mouse Left Pressed!");

	//check if no students at mouse click
	if(not position_meeting(mouse_x, mouse_y, obj_student))
	{
		//If you have a student selected, unselect
		if(has_student)
		{
			has_student = false;
			with(current_student)
			{
				selected = false;
			}
		}
		//Otherwise do nothing
	}
	//if there is a student at mouse click
	else
	{
		var student_clicked = instance_position(mouse_x, mouse_y, obj_student)
		var new_student_info = student_clicked.my_info;
		//if you re-clicked your current selection, unselect it
		if(has_student and new_student_info.id == current_student_info.id)
		{
			//show_debug_message("Same Student Selected")
			has_student = false;
			with(current_student)
			{
				selected = false;
			}
		}
		else
		{
			//Select student clicked on to activate highlight
			with(student_clicked)
			{
				selected = true;
			}
			//unselect previous student if exists
			if(has_student)
			{
				with(current_student)
				{
					selected = false;
				}
			}
			//record newly selected student
			current_student = student_clicked;
			current_student_info = new_student_info;
			has_student = true;
		}
	}
}