/// @description Insert description here
// You can write your code in this editor
if(has_student and instance_exists(current_student))
{
	draw_rectangle(LEFT_BOUND, UPPER_BOUND, RIGHT_BOUND, LOWER_BOUND, true);
	//Draw sprite on center left,
	draw_sprite_ext(spr_students, current_student.sprite_choice, LEFT_BOUND + WIDTH * 0.07, LOWER_BOUND - HEIGHT * 0.2, current_student.x_scale, current_student.y_scale, current_student.image_angle, current_student.blend_color, current_student.image_alpha); 
	//Draw vertical line separating portrait from info
	draw_line(LEFT_BOUND + WIDTH * 0.14, UPPER_BOUND, LEFT_BOUND + WIDTH * 0.14, LOWER_BOUND);
	//Draw name on center top
	draw_text(LEFT_BOUND + WIDTH * 0.15, UPPER_BOUND + HEIGHT * 0.1, "Name: " + current_student.name);
	//Draw origin class center center
	draw_text(LEFT_BOUND + WIDTH * 0.15, UPPER_BOUND + HEIGHT * 0.35, "Just left classroom: " + string(current_student.origin_class));
	//Draw destination class center bottom;
	draw_text(LEFT_BOUND + WIDTH * 0.15, UPPER_BOUND + HEIGHT * 0.6, "On their way to: " + string(current_student.destination_number));
	//Draw Interests upper right
	draw_text(LEFT_BOUND + WIDTH * 0.51, UPPER_BOUND + HEIGHT * 0.1, "Interests: " + global.enum_interests[current_student.interest]);
	//Draw favorite subject center right
	draw_text(LEFT_BOUND + WIDTH * 0.51, UPPER_BOUND + HEIGHT * 0.35, "Favorite Subject: " + global.enum_subjects[current_student.fav_subject]);
	//Draw best friend bottom right
	draw_text(LEFT_BOUND + WIDTH * 0.51, UPPER_BOUND + HEIGHT * 0.6, "Best Friend: " + current_student.best_friend_info.name);

}