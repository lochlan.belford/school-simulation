/// @description Clean up and return to main game

//Destroy all buttons
for(var b = 0; b < ds_list_size(buttons); b++)
{
	var button = ds_list_find_value(buttons,b)
	if(instance_exists(button))
	{
		with(button)
		{
			instance_destroy();
		}
	}
}
//clean up data structures
ds_list_destroy(buttons);

//Call player's end interaction method
var destroy = destroy_player
with(player)
{
	end_interaction();
	//Some interactions mark the player for destruction--do this now, after clearing
	if destroy
	{
		instance_destroy();
	}
}


//unpause game
global.interaction_in_progress = false;
global.paused = false;

//Goodbye! (parent destroy)
event_inherited();