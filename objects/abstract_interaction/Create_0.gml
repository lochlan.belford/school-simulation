/// @description Enter interaction state
// You can write your code in this editor
event_inherited();

show_line_numbers = false;

global.paused = true;
global.interaction_in_progress = true;

//All occur in the same place relative to viewport
x = camera_get_view_x(view_camera[0]) + global.NOTECARD_X;
y = camera_get_view_y(view_camera[0]) + global.NOTECARD_Y;

//all interactions involve the player, so get them
//This feels sorta hacky, wouldn't work for multiplayer
player = instance_find(obj_player_student,0);

//They also all involve the player's target, so get that
target = player.current_target;

//They all involve at least one button
buttons = ds_list_create();

//sometimes the player gets destroyed, record to do during interaction's destroy
destroy_player = false;

//Create an x in upper left to close every interaction
close_button = instance_create_layer(bbox_left + 8, bbox_top + 8, "interactions",abstract_button)
close_button.build_button(spr_pencil_x);
ds_list_add(buttons, close_button);