/// @description Insert description here

width = 700;
height = 200;

num_rows = 3;
num_columns = 4;

names = ds_list_create();

function build_panel(w,h,students)
{
	height = h;
	width = w;
	for(var s = 0; s < ds_list_size(students); s++)
	{
		var next_student = ds_list_find_value(students,s);
		ds_list_add(names,next_student.name);
	}
}