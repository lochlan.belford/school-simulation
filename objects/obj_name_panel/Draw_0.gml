/// @description Insert description here

var names_drawn = 0;
var current_col = 0;
var current_row = 0;
while(names_drawn < ds_list_size(names))
{
	var next_name = ds_list_find_value(names,names_drawn);
	draw_set_font(fnt_paper_med)
	draw_set_valign(fa_bottom)
	draw_set_halign(fa_left)
	draw_text_color(x + width * (current_col / num_columns), y + height * (current_row / num_rows),next_name,c_black,c_black,c_black,c_black,1);
	names_drawn += 1;
	if(current_row < num_rows - 1)
	{
		current_row += 1;
	}
	else if(current_col < num_columns - 1)
	{
		current_col += 1;
		current_row = 0;
	}
	//should never reach this point, would start overwriting
	else
	{
		current_col = 0;
		current_row = 0;
	}
}