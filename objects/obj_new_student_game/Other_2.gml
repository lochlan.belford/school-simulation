/// @description Creates Student Roster

//StudentInfo constructor--defines most attributes randomly at time of construction
StudentInfo = function(_id, _name, _schedule) constructor
{
	id = _id;
	name = _name;
	schedule = _schedule
	classes_visited = 0;
	sprite_choice = irandom(global.STD_MAX_SPRITES);
	xscale = random_range(global.STD_MIN_XSCALE, global.STD_MAX_XSCALE);
	yscale = random_range(global.STD_MIN_YSCALE, global.STD_MAX_YSCALE);
	hue = irandom_range(global.STD_MIN_HUE, global.STD_MAX_HUE);
	sat = irandom_range(global.STD_MIN_SAT, global.STD_MAX_SAT);
	lum = irandom_range(global.STD_MIN_LUM, global.STD_MAX_LUM);
	//Create an array of length Interests.length, fill with values representing favorability of each interest
	interests = ds_list_create();
	for(var i = 0; i < INTERESTS.length; i++)
	{
		ds_list_add(interests, irandom_range(global.STD_MIN_OPINION, global.STD_MAX_OPINION));
	}
	subjects = ds_list_create();
	for(var s = 0; s < SUBJECTS.length; s++)
	{
		ds_list_add(subjects, irandom_range(global.STD_MIN_OPINION, global.STD_MAX_OPINION));
	}
	//Create a map to hold relationship values with other students, but leave empty until player meets them
	relationships = ds_map_create();
	locker_number = 101;
	known_to_player = false;
	
}

//Generate Students
//Create empty roster, list of StudentInfo structs
student_roster = ds_list_create();

//Read all names from names file
names_file = file_text_open_read("random_names_1.txt");
all_names = ds_list_create();
while(not file_text_eof(names_file))
{
	var next_name = file_text_read_string(names_file);
	next_name = string_letters(next_name);
	ds_list_add(all_names, next_name);
	file_text_readln(names_file);
}
//close file
file_text_close(names_file);
//Shuffle order of names
ds_list_shuffle(all_names);


//Create basic schedule to be randomized for each student
schedule_template = ds_list_create();
ds_list_add(schedule_template, 101, 102, 103, 104);


//fill roster with students up to constant-defined max number of students
for(var s = 0; s < global.STD_NUM_STUDENTS; s++){
	//give each student the next name in the shuffled name list
	var name_choice = ds_list_find_value(all_names, s);
	//Give each student a shuffled copy of the default class schedule
	var schedule_choice = ds_list_create();
	ds_list_copy(schedule_choice, schedule_template);
	ds_list_shuffle(schedule_choice);
	//All other values are rolled randomly within the constructor
	var next_student = new StudentInfo(s, name_choice, schedule_choice);
	ds_list_add(student_roster, next_student);
}