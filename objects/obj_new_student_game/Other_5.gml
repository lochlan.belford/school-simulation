/// @description Saves Created Char Details

//If in rm_create_char
//save character creation values
if (room == rm_create_char)
{
	PlayerInfo = function(_name, _shape, _x, _y, _hue, _sat, _lum, _schedule) constructor
	{
		id = global.STD_NUM_STUDENTS; //the player is the last one on the roster
		name = _name;
		sprite_choice = _shape;
		xscale = _x;
		yscale = _y;
		hue = _hue;
		sat = _sat;
		lum = _lum;
		//Create an array of length Interests.length, fill with values representing favorability of each interest
		interests = ds_list_create();
		for(var i = 0; i < INTERESTS.length; i++)
		{
			ds_list_add(interests, irandom_range(global.STD_MIN_OPINION, global.STD_MAX_OPINION));
		}
		subjects = ds_list_create();
		for(var s = 0; s < SUBJECTS.length; s++)
		{
			ds_list_add(subjects, irandom_range(global.STD_MIN_OPINION, global.STD_MAX_OPINION));
		}
		classes_visited = 0;
		schedule = _schedule;
		//Create a map to hold relationship values with other students, but leave empty until player meets them
		relationships = ds_map_create();
		locker_number = 101;
		known_to_player = true;
		names_in_order = ds_list_create();
	
	}


	var char_creator = instance_find(obj_character_creator, 0);
	var schedule_choice = ds_list_create();
	ds_list_copy(schedule_choice, schedule_template);
	ds_list_shuffle(schedule_choice);
	
	//Player data is maintained globally to avoid having copies--update everything here
	global.player_data = new PlayerInfo(char_creator.name, char_creator.shape_index, char_creator.x_scale, char_creator.y_scale, char_creator.hue, char_creator.sat, char_creator.lum, schedule_choice);
	
}