/// @description Defines Game Constants
/*
This object contains the highest level game logic for my first goal-oriented game using the school simulation
In "The New Student," players
	- Begin the game 
		- Title screen
		- Options?
		- Game Instructions
	- Create a character
		- Name
		- Appearance
		- Personality
	- Spawn in a school hallway with 160 NPC students
		- Students all have names, schedules, and interests
		- Initially, these are all opaque to the player
	- Are given a tutorial
		- How to follow your class schedule
		- How to introduce yourself to other students
	- Go to their first class
		- Narrative summary of the class
		- "You learned the names of X more students:"
	- Play through three more class changes and classes
		- Goal is to meet other students and learn things about them
		- Names appear above previously met students in the world
		- Target panel displays known information
	- End the day, view summary
		- Screen featuring all students they met
		- Fills in web of relationships
		- Fills in more inforamtion to demonstrate depth?
*/

//Randomize the random seed
randomize();

//Define Constants

//Students
global.STD_NUM_STUDENTS = 159; //The total number of students to generate
global.STD_MAX_SPRITES = 2; //The number of base sprites to choose from
//Minimum and maximum values for student height and width

global.STD_MIN_XSCALE = 0.25;
global.STD_MAX_XSCALE = 0.75;
global.STD_MIN_YSCALE = 0.375;
global.STD_MAX_YSCALE = 1.0;
//Minimimum and maximum values for student color blend components
global.STD_MIN_HUE = 0;
global.STD_MAX_HUE = 255;
global.STD_MIN_SAT = 0;
global.STD_MAX_SAT = 255;
global.STD_MIN_LUM = 0;
global.STD_MAX_LUM = 255;
//Minimum and maximum values for opinion and friendship ratings
global.STD_MIN_OPINION = -5;
global.STD_MAX_OPINION = 10;
global.STD_MIN_FRIENDSHIP = -3;
global.STD_MAX_FRIENDSHIP = 10;

global.ROOM_SPEED_STANDARD = 60;
global.ROOM_SPEED_FAST = 120;

global.CLASS_CHANGE_LENGTH = 90;

global.NOTECARD_X = 356;
global.NOTECARD_Y = 242;

global.PAPER_X = 203;
global.PAPER_Y = 291;