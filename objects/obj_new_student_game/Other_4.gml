/// @description Inits "School Day" object

//if in rm_new_student_hall
//create obj_school_day
//pass data
if(room == rm_new_student_hall)
{
	//define room constants
	global.HALL_LEFT = 0;
	global.HALL_RIGHT = room_width;
	global.HALL_TOP = room_height / 2;
	global.HALL_BOTTOM = room_height - 8;
	global.HALL_NUM_CLASSROOMS = 4;
	global.HALL_DOOR_WIDTH = 128;
	global.HALL_DOOR_HEIGHT = 256;
	global.HALL_NUM_SPAWNS = 3;
	global.VIEWPORT_WIDTH = 1366
	global.STD_SPAWN_RATE = room_speed;
	
	
	school_day = instance_create_layer(0,0,"school_day_controller",obj_school_day);
	school_day.student_roster = student_roster;
	with(school_day)
	{
		begin_day();
	}
}