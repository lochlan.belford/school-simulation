/// @description Define locker text


// Inherit the parent event
event_inherited();

title = "Your locker"

var class_number_to_title = ds_map_create()
ds_map_add(class_number_to_title,101,"Math")
ds_map_add(class_number_to_title,102,"Social Studies")
ds_map_add(class_number_to_title,103,"English")
ds_map_add(class_number_to_title,104,"Science")
var player_schedule = global.player_data.schedule;

//say which book you are taking
var book_number = ds_list_find_value(player_schedule,global.player_data.classes_visited);
var book = ds_map_find_value(class_number_to_title,book_number)

player.has_book = true;


//Just say you get your things
ds_list_set(lines,1,"You get your textbook");
ds_list_set(lines,2,"for " + book + " class.");


//Set self destruct to 2 seconds
alarm[0] = 2.5 * global.ROOM_SPEED_STANDARD;

