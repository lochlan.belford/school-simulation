/// @description Insert description here
// You can write your code in this editor
//Follow view's x position
x = camera_get_view_x(view_camera[0]) + destination_x;
if(!global.paused and keyboard_check_pressed(vk_escape))
{
	show_menu(card_index);
}
else if(global.paused and !global.interaction_in_progress and keyboard_check_pressed(vk_escape))
{
	hide_menu();
}
if(enter_screen)
{
	y -= travel_speed;
	if(y <= destination_y)
	{
		y = destination_y;
		enter_screen = false;
		on_screen = true;
		menu_activate();
	}
}
else if(leave_screen)
{
	y+= travel_speed
	//made it off the screen
	if(y>=origin_y)
	{
		y = origin_y;
		leave_screen = false;
		off_screen = true;
	}
}

//Card switching test
if(on_screen and keyboard_check_pressed(vk_right))
{
	next_card();
}
else if(on_screen and keyboard_check_pressed(vk_left))
{
	prev_card();
}

