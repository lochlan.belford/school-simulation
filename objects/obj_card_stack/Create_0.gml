/// @description Data Structure for cards, constants
// You can write your code in this editor

//Tracks cards as simple enum, only creates them when they are on top of stack
enum CARDS{
	SCHEDULE,
	LOCKER,
	TUTORIAL,
	STUDENTS_MET,
	length
}
top_card = undefined;
card_index = CARDS.TUTORIAL;

origin_y = y;
destination_x = 356
destination_y = 100 + 142
destination_epsilon = 10;
enter_screen = false;
on_screen = false;
leave_screen = false;
off_screen = true;

title = "Menu"


travel_speed = 20 * room_speed / global.ROOM_SPEED_STANDARD;

function show_menu(index)
{
	if(not leave_screen and not enter_screen and not on_screen)
	{
		global.paused = true;
		enter_screen = true;
		reveal_card(index)
	}
	
}

function menu_activate()
{
	//calculate new button positions
	var cycle_y = (camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0])) / 2;
	var next_x = (camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0])) - 96;
	var prev_x = camera_get_view_x(view_camera[0]) + 96;
	var return_x = camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) / 2
	var return_y = (camera_get_view_y(view_camera[0]) + camera_get_view_height(view_camera[0])) - 64
	var exit_x = camera_get_view_x(view_camera[0]) + 128;
	var exit_y = camera_get_view_y(view_camera[0]) + 32
	//move buttons to correct positions
	next_button.x = next_x;
	next_button.y = cycle_y;
	prev_button.x = prev_x;
	prev_button.y = cycle_y;
	return_button.x = return_x;
	return_button.y = return_y;
	exit_button.x = exit_x;
	exit_button.y = exit_y;
	//then activate
	instance_activate_object(next_button)
	instance_activate_object(prev_button)
	instance_activate_object(return_button)
	instance_activate_object(exit_button)
}

function menu_deactivate()
{
	instance_deactivate_object(next_button)
	instance_deactivate_object(prev_button)
	instance_deactivate_object(return_button)
	instance_deactivate_object(exit_button)
}

function hide_menu()
{
	menu_deactivate();
	if(not leave_screen and not enter_screen)
	{
		global.paused = false;
		leave_screen = true;
		on_screen = false;
	}
}

function next_card()
{
	if(card_index < CARDS.length - 1)
	{
		card_index += 1;
	}
	else
	{
		card_index = 0;
	}
	reveal_card(card_index);
}

function prev_card()
{
	if(card_index > 0)
	{
		card_index -= 1;
	}
	else
	{
		card_index = CARDS.length - 1;
	}
	reveal_card(card_index);
}

/* reveal_card(card_enum_index)
creates and displays appropriate card based on enum index
Destroys old card
*/
function reveal_card(index)
{
	//destroy old card if exists
	if(not is_undefined(top_card) and instance_exists(top_card))
	{
		instance_destroy(top_card);
	}
	switch(index)
	{
		case CARDS.SCHEDULE:
			top_card = instance_create_layer(x,y,"menus",obj_schedule_card);
			break;
		case CARDS.LOCKER:
			top_card = instance_create_layer(x,y,"menus",obj_locker_card);
			break;
		case CARDS.STUDENTS_MET:
			top_card = instance_create_layer(x,y,"menus",obj_names_card);
			break;
		//default to tutorial cards
		case CARDS.TUTORIAL:
		default:
		top_card = instance_create_layer(x,y,"menus",obj_tutorial_card);
			break;
	}
}

//Reveal the first card at end of create event
reveal_card(card_index)

//button functions
function next()
{
	obj_card_stack.next_card()
}

function prev()
{
	obj_card_stack.prev_card()
}
function return_to_game()
{
	obj_card_stack.hide_menu();
}
function exit_game()
{
	restart_game();
}
function menu()
{
	with(obj_card_stack)
	{
		show_menu(card_index);
	}
}

//create menu buttons, then disable
//next button
next_button = instance_create_layer(x,y,"menus",abstract_button);
next_button.build_button_full(spr_button_small,"Next ->",next)
//prev button
prev_button = instance_create_layer(x,y,"menus",abstract_button);
prev_button.build_button_full(spr_button_small,"<- Prev",prev)
//return button
return_button = instance_create_layer(x,y,"menus",abstract_button);
return_button.build_button_full(spr_button_med,"Return to Game",return_to_game)
//exit game
exit_button = instance_create_layer(x,y,"menus",abstract_button);
exit_button.build_button_full(spr_button_med,"Exit Game",exit_game)

menu_deactivate();

//create button to go to menu in main layer, always active
menu_button = instance_create_layer(x,y,"hallway_ui",abstract_button)
menu_button.build_button_full(spr_button_med,"Menu (Esc)",menu);