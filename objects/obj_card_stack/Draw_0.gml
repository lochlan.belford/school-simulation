/// @description Drawing full stack of notecards
// You can write your code in this editor


//Draw Stack shadows
for(var shadow = 4; shadow > 0; shadow--)
{
	draw_sprite(sprite_index,1,x+(5*shadow),y+(5*shadow));
}

//Above dimmed screen and other cards
draw_self();

//draw "Menu" above cards
if(on_screen)
{
	draw_set_font(fnt_paper_main_title)
	draw_set_valign(fa_middle)
	draw_set_halign(fa_center)
	draw_text_color(camera_get_view_x(view_camera[0]) + global.VIEWPORT_WIDTH/2, 64,title,c_black,c_black,c_black,c_black,1);
}