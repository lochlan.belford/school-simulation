/// @description Set up shared button handling
// You can write your code in this editor


//can be assigned a function to handle on click, default none
my_function = undefined

button_text = "";

//Always make depth slightly closer that whatever spawned it
depth -= 1


//three options for customizing button appearance and function
function build_button_full(sprite,text,func)
{
	sprite_index = sprite;
	button_text = text;
	my_function = func;
}

function build_button_function(sprite,func)
{
	sprite_index = sprite;
	my_function = func;
}

function build_button_text(sprite,text)
{
	sprite_index = sprite;
	button_text = text;
}

function build_button(sprite)
{
	sprite_index = sprite;
}