/// @description Shared default/hover/click drawing
// You can write your code in this editor
draw_self();
draw_set_font(fnt_paper_med)
draw_set_halign(fa_center)
draw_set_valign(fa_middle)
if(image_index == BUTTON_STATES.HOVER)
{
	draw_set_color(c_black);
}
else
{
	draw_set_color(c_white)
}
draw_text(x,y,button_text);
draw_set_color(c_white);