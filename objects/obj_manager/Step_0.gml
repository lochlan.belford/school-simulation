/// @description Insert description here
// You can write your code in this editor

//count how many students have spawned
var total_spawned = 0;
for(var c = 0; c < instance_number(obj_classroom); c++)
{
	var classroom = instance_find(obj_classroom, c);
	total_spawned += classroom.students_spawned;
}
//count how many students are left in hallway
var students_in_hall = instance_number(obj_student);
//Once all students have spawned AND gone to their next classroom, reset each classroom's student list
if(students_in_hall <= 0 and total_spawned >= RM_NUM_CLASSROOMS * RM_STUDENTS_PER_CLASS)
{
	for(var c = 0; c < instance_number(obj_classroom); c++)
	{
		var classroom = instance_find(obj_classroom, c);
		with(classroom)
		{
			ds_list_copy(students, arriving_students);
			students_spawned = 0;
			started_spawning = false;
			continue_spawning = false;
			num_old_students = ds_list_size(students);
			spawn_rate = SECONDS_TO_SPWAN_ALL / num_old_students * room_speed;
			ds_list_destroy(arriving_students);
			arriving_students = ds_list_create();
			num_new_students = 0;
		}
	}
	late_for_class = false;
	alarm[0] = 120 * room_speed;
}

if(global.paused)
{
	alarm[0] += 1;
}