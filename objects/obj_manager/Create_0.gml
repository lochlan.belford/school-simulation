/// @description Insert description here
// You can write your code in this editor

//show_debug_message("obj_manager created. Hello!");
randomize();
//random_set_seed(1501971202);

//Create students and assign to rooms
//More specifically, create the VALUES that define a student, store in array of arrays
/* Students:
	id
	name
	sprite
	y_scale 
	x_scale
	tint
*/
//Student Constants
STD_NUM_STUDENTS = 1600;
STD_MAX_SPRITES = 2;
STD_MIN_YSCALE = 0.75;
STD_MIN_XSCALE = 0.5;
STD_MAX_YSCALE = 2.0;
STD_MAX_XSCALE = 1.5;
STD_MIN_HUE = 0;
STD_MAX_HUE	= 255;
STD_MIN_SAT = 0;
STD_MAX_SAT = 255;
STD_MIN_LUM = 0;
STD_MAX_LUM = 255;

//Student constructor
Student = function(_id, _name, _sprite, _xscale, _yscale, _hue, _sat, _lum, _interest, _subject, _friend_id, _manager_id, _schedule) constructor
{
	id = _id;
	name = _name;
	sprite = _sprite;
	xscale = _xscale;
	yscale = _yscale;
	hue = _hue;
	sat = _sat;
	lum = _lum;
	interest = _interest;
	fav_subject = _subject;
	best_friend_id = _friend_id;
	manager = _manager_id;
	schedule = _schedule
	classes_visited = 1;
}
//Read all names from names file
names_file = file_text_open_read("random_names_1.txt");
all_names = ds_list_create();
while(not file_text_eof(names_file))
{
	var next_name = file_text_read_string(names_file);
	next_name = string_letters(next_name);
	ds_list_add(all_names, next_name);
	file_text_readln(names_file);
}
//close file
file_text_close(names_file);
//Shuffle order of names
ds_list_shuffle(all_names);

//Trying LIST of all students
all_students = ds_list_create();
var schedule_template = ds_list_create();
ds_list_add(schedule_template, 101, 102, 103, 104);
//I hear it is most efficient to fill backward, but for now forward
for(var s = 0; s < STD_NUM_STUDENTS; s++){
	//fill in id, generate random
	var sprite_choice = irandom(STD_MAX_SPRITES);
	var x_choice = random_range(STD_MIN_XSCALE,	STD_MAX_XSCALE);
	var y_choice = random_range(STD_MIN_YSCALE, STD_MAX_YSCALE);
	var hue_choice = irandom_range(STD_MIN_HUE, STD_MAX_HUE);
	var sat_choice = irandom_range(STD_MIN_SAT, STD_MAX_SAT);
	var lum_choice = irandom_range(STD_MIN_LUM, STD_MAX_LUM);
	var name_choice = ds_list_find_value(all_names, s);
	var interest_choice = irandom(INTERESTS.length - 1);
	var subject_choice = irandom(SUBJECTS.length - 1);
	var friend_id_choice = irandom(STD_NUM_STUDENTS - 1);
	var schedule_choice = ds_list_create();
	ds_list_copy(schedule_choice, schedule_template);
	ds_list_shuffle(schedule_choice);
	var next_student = new Student(s, name_choice, sprite_choice, x_choice, y_choice, hue_choice, sat_choice, lum_choice, interest_choice, subject_choice, friend_id_choice, id, schedule_choice);
	ds_list_add(all_students, next_student);
}

////Iterate through again andturn friend ids into object ids (DEBUG: yikes!)
//for(var f = 0; f < STD_NUM_STUDENTS; f++)
//{
//	var student = ds_list_find_value(all_students, f);
//	var friend = ds_list_find_value(all_students, student.best_friend_id);
//	student.best_friend = friend;
//	//Do I need to re-insert student into list? Or am I editing the underlying object/ (DEBUG: plz)
//}

//show_debug_message(json_encode(all_students));

//Now Trying MAP of relationships (DEBUG: eeep!)
//global.relationships = ds_map_create();
////Now just simply fill 2.6 million entries with a random number
//for(var i = 0; i < STD_NUM_STUDENTS; i++)
//{
//	//see just how slow this is
//	show_debug_message("Finished relationships for student: " + string(i));
//	for(var j = 0; j < STD_NUM_STUDENTS; j++)
//	{
//		//Don't give students relationships with themselves
//		if(i != j)
//		{
//			var relationship_tuple = string(i) + string(j);
//			//Randomly assign each relationship a value between 0 and 9)
//			ds_map_add(global.relationships, relationship_tuple, irandom(9));
//		}
//	}
//}

//Spawn classrooms and hallway
RM_NUM_CLASSROOMS = 4;
RM_NUM_HALLS = 1;
//DEBUG: Can I retrieve these programmatically?
RM_DOOR_WIDTH = 128;
RM_DOOR_HEIGHT = 256;

//DEBUG: Set to 400 to test full school, 40 to test one hall
RM_STUDENTS_PER_CLASS = 40;

hall_y = room_height / (RM_NUM_HALLS + 1);
for(var hall_x = 0; hall_x < room_width; hall_x += RM_DOOR_WIDTH)
{
	instance_create_layer(hall_x, hall_y, "hallway", obj_hallway);
}

classroom_map = ds_map_create();
classroom_spacing = room_width / (RM_NUM_CLASSROOMS + 1);
classroom_x = classroom_spacing;
classroom_y = hall_y;
//Shuffle list of students to get random assortment to add to classrooms
ds_list_shuffle(all_students);

//Create the classrooms
for(var c = 0; c < RM_NUM_CLASSROOMS; c++)
{
	new_classroom = instance_create_layer(classroom_x, classroom_y, "hallway", obj_classroom);
	ds_map_add(classroom_map, new_classroom.room_number, new_classroom);
	classroom_x += classroom_spacing;
	//Creat list of students for new classroom, fill with next 40 students
	//var selected_students = ds_list_create()
	//for(var s = 0; s < RM_STUDENTS_PER_CLASS; s++){
	//	var next_student = ds_list_find_value(all_students, next_student_id);
	//	ds_list_add(selected_students, next_student);
	//	next_student_id += 1;
	//}
	//with(new_classroom){
	//	ds_list_copy(students, selected_students);
	//	num_old_students = ds_list_size(students);
	//}
}
//get first num_classroom * num_students_per_classroom from list of all students
//Place them all in their first classroom, with hopefully near-uniform distribution
for(var s = 0; s < RM_NUM_CLASSROOMS * RM_STUDENTS_PER_CLASS; s++)
{
	var next_student = ds_list_find_value(all_students, s);
	var their_first_classroom = classroom_map[? ds_list_find_value(next_student.schedule, 0)];
	with(their_first_classroom)
	{
		ds_list_add(students, next_student);
		num_old_students = ds_list_size(students);
		spawn_rate = SECONDS_TO_SPWAN_ALL / num_old_students * room_speed;
	}
}

//Create info panel
//show_debug_message("Creating Info Panel");
instance_create_layer(0,0,"students",obj_info_panel);

//Create "bell ring" alarm to force all students into classrooms after 2 minutes
late_for_class = false;
alarm[0] = 120 * room_speed;

//DEBUG: Saves Generated students into a json list (hopefully)
//students_string = ds_list_write(all_students);
//show_debug_message(students_string);
//student_file_handle = file_text_open_write(working_directory + "students.txt");
//file_text_write_string(student_file_handle, students_string);
//file_text_close(student_file_handle);
//show_debug_message("Working Directory is: " + working_directory);