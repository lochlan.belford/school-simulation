/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

title = "How to Play"

ds_list_set(lines,0,"Click to move and interact.");
ds_list_set(lines,1,"Arrows and WASD also move.");
ds_list_set(lines,2,"Click on doors to go to class.");
ds_list_set(lines,3,"Click on your locker to open it.");
ds_list_set(lines,4,"Click on students to meet them.");
ds_list_set(lines,5,"Meet as many as you can!");



