/// @description Player movement
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var right = keyboard_check(vk_right) or keyboard_check(ord("D"));
var left = keyboard_check(vk_left) or keyboard_check(ord("A"));
var up = keyboard_check(vk_up) or keyboard_check(ord("W"));
var down = keyboard_check(vk_down) or keyboard_check(ord("S"));
var key_moving = (up xor down) or (left xor right);

//TODO: Consider adding gesture events
var click = mouse_check_button_pressed(mb_any);

if global.paused
{
	accept_input = false;
	//wait just a couple frames before accepting input again
	alarm[5] = 2;
}

//check that target has not been destroyed
if(has_target and not instance_exists(current_target))
{
	clear_target()
}

if not global.paused and accept_input
{
	//Keyboard movement has priority and clears preexisting destinations and targets
	if key_moving
	{
		clear_target();
		speed = 5;
		//up group
		if(up and right)
		{
			direction = 45;
		}
		else if(up and left)
		{
			direction = 135;
		}
		else if(up)
		{
			direction = 90;
		}
		//down group
		else if(down and right)
		{
			direction = -45;
		}
		else if(down and left)
		{
			direction = -135;
		}
		else if(down)
		{
			direction = -90;
		}
		//just right or left
		else if(right)
		{
			direction = 0;
		}
		else if(left)
		{
			direction = 180;
		}
	}
	//If not key moving, Handle Click Priority System
	else if click
	{
		clear_target();
		//Check for buttons, students, classrooms, lockers
		var clicked_button = instance_position(mouse_x,mouse_y,abstract_button);
		var clicked_student = instance_position(mouse_x,mouse_y,obj_npc_student);
		var clicked_classroom = instance_position(mouse_x,mouse_y,obj_classroom);
		var clicked_locker = instance_position(mouse_x,mouse_y,obj_locker);
		//Handle them in that priority order
		if clicked_button != noone
		{
			//Do nothing, this is here to not interact with anything under the button
		}
		else if clicked_student != noone
		{
			speed = 5;
			set_target(clicked_student);
		}
		else if clicked_classroom != noone
		{
			speed = 5;
			set_target(clicked_classroom);
		}
		else if clicked_locker != noone
		{
			speed = 5;
			set_target(clicked_locker);
		}
		else //clicked on empty space, just start moving there
		{
			speed = 5;
			set_destination(mouse_x,mouse_y);
		}
	}
	//If no new input, next check for target and interaction
	else if has_target
	{
		//track it's destination (it may have moved, in the case of student)
		set_destination(current_target.x, current_target.y)
		//If at target, interact with it if haven't yet
		if target_reached(current_target) and not interaction_initiated
		{
			interact(current_target)
		}
	}
	//If no target, check if reached destination
	else if has_destination and abs(x-destination_x) < destination_epsilon and abs(y-destination_y) < destination_epsilon{
		speed = 0;
		has_destination = false;
	}
	//finally, not moving or has destination, totally at rest
	else if not has_destination
	{
		speed = 0;
	}

	//check bounds
	//Check for collision with upper and lower bounds of hallway
	if(y < global.HALL_TOP) //This is confusing because y increases downward
	{
		y = global.HALL_TOP;
		speed = 0;
	}
	else if(y > global.HALL_BOTTOM)
	{
		y = global.HALL_BOTTOM;
		speed = 0;
	}
}