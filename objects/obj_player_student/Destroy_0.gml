/// @description Pass Data, clean up highlight
// You can write your code in this editor

//target should already be cleared in all possible methods of destruction, but just in case
clear_target();

//destroy highlight
with(my_highlight)
{
	instance_destroy();
}

// Inherit the parent event (last for garbage collection)
event_inherited();

