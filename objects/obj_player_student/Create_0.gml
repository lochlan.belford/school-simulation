/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

destination_x = 0;
destination_y = 0;

//Flag set when player has an x,y point to travel toward
has_destination = false;

//Flag set when player has selected a classroom, locker, or student to move to and interact with
has_target = false;
//Holds reference to classroom, locker, or student
current_target = undefined;
//How many pixels away from target interaction is allowed
target_epsilon_y = 25
target_epsilon_x = 100
//Flag set when interaction window pops up for classroom or student
interaction_initiated = false

has_book = false;

accept_input = true;


function spawn_highlight(){
	my_highlight = instance_create_layer(x,y,"highlights",obj_player_highlight);
	my_highlight.image_index = global.player_data.sprite_choice;
	my_highlight.image_xscale = global.player_data.xscale;
	my_highlight.image_yscale = global.player_data.yscale;
}


/* set_target(obj)
Sets target to given instance, sets destination, sets flags
*/
function set_target(instance){
	clear_target();
	current_target = instance;
	has_target = true;
	set_destination(instance.x, instance.y);
	//all eligible targets have a "selected" instance variable for highlighting
	with(current_target)
	{
		selected = true;
	}
}

/* set_destination(x,y)
Flags has_destination, sets x and y
*/
function set_destination(_xx,_yy)
{
	has_destination = true;
	destination_x = _xx;
	destination_y = _yy;
	direction = point_direction(x,y,destination_x,destination_y);
}

/* clear_target()
clears target, destination, and flags
Mostly here if I've written code that freezes current target or something, and want to release
*/
function clear_target()
{
	if not is_undefined(current_target)
	{
		with(current_target)
		{
			selected = false;
		}
	}
	current_target = undefined;
	has_target = false;
	has_destination = false;
}

/* target_reached()
returns true if within target_epsilon pixels of target location, false otherwise
*/
function target_reached(target)
{
	if(abs(x - target.x) <= target_epsilon_x and abs(y - target.y) <= target_epsilon_y)
	{
		return true;
	}
	return false;
}

//UNDER CONSTRUCTION
/* interact(target)
	calls specific interact method based on the type of target
	(Student, Locker, Classroom)
*/
function interact(target){
	show_debug_message("PLAYER INTERACTION TRIGGERED")
	interaction_initiated = target.player_interaction();
	//If this interaction failed, end here
	if not interaction_initiated
	{
		end_interaction();
	}
	//otherwise further steps are handled by the interaction object
}

/* end_interaction()
	As currently designed, interactions must call this as part of their destroy method
	(Seems maybe not great)
*/
function end_interaction()
{
	clear_target();
	interaction_initiated = false;
}

function close_locker(locker)
{
	locker_to_close = locker;
	alarm[10] = 1 * global.ROOM_SPEED_STANDARD
}