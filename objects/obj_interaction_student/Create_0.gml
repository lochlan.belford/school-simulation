/// @description Insert description here


// Inherit the parent event
event_inherited();

//Title is student name
title = target.name

cell_width = 131

//determine conversation to have
conversation_type = 0;

//conversation animation takes up 5 lines
animation = instance_create_layer(x,y,"animations",obj_conversation_animation)
animation.build_animation(line_height*5,cell_width*5,player.my_info,target.my_info,conversation_type)

//one sentence description At bottom of card
ds_list_set(lines,5,"You met " + target.name + "!");

//Set self-destruct to 2.5 seconds
alarm[0] = 2.5 * global.ROOM_SPEED_STANDARD;