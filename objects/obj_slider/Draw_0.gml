/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_set_font(fnt_paper_small)
draw_set_halign(fa_left);
draw_set_valign(fa_top);
//Draw title above center of bar
draw_text_color(bbox_right - ((bbox_right-bbox_left)/2) - 20, bbox_top - 20, title,c_black,c_black,c_black,c_black,1);

//Draw min and max values below ends of bar
draw_text_color(bbox_left, bbox_bottom + 10, string(min_val),c_black,c_black,c_black,c_black,1);
draw_text_color(bbox_right - 20, bbox_bottom + 10, string(max_val),c_black,c_black,c_black,c_black,1);

//Draw current value below middle of bar
draw_text_color(bbox_right - ((bbox_right-bbox_left)/2) - 20, bbox_bottom + 10, string(current_val),c_black,c_black,c_black,c_black,1);