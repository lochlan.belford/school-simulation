/// @description Insert description here
// You can write your code in this editor

/*
A slider object contains: 
	* a slider handle, which starts at the sprite origin (middle left)
	* A minimum value
	* A maximum value
	* A display of the current value
*/

handle = instance_create_layer(x,y,"Instances",obj_slider_handle)
with(handle)
{
	depth = -100;
}
min_val = 0;
max_val = 100;
title = "Test";
round_val = false;
//show_debug_message("bbox_right = " + string(bbox_right));

function calc_val(){
	//Get percentage of bar traversed by handle
	var handle_position = handle.x - self.x
	var total_width = bbox_right - bbox_left;
	var percent = handle_position / total_width;
	//Multiply by max - min to get current val
	var answer = min_val + (max_val - min_val) * percent;
	if round_val
	{
		return floor(answer)
	}
	else
	{
		return answer
	}
}

current_val = calc_val()