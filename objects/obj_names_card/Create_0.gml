/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

title = "People I've Met"

//get all names from name list
for(var n = 0; n < ds_list_size(global.player_data.names_in_order); n++)
{
	var next_name = ds_list_find_value(global.player_data.names_in_order,n);
	ds_list_set(lines,n,next_name);
}
