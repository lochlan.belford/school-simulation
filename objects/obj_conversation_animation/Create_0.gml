/// @description Constructor

height = 300;
width = 500;
/* build_anmiation(abstract_student.my_info, abstract_student.my_info)
*/
function build_animation(h,w,speaker_1_info,speaker_2_info,type)
{
	height = h;
	width = w;
	speaker_1 = instance_create_layer(x + width*(1/4),y+height*(4/5),"animations",obj_puppet_student)
	speaker_1.init_from_info(speaker_1_info);
	speaker_2 = instance_create_layer(x + width*(3/4),y+height*(4/5),"animations",obj_puppet_student)
	speaker_2.init_from_info(speaker_2_info);
	first_speaker = choose(speaker_1, speaker_2);
	if(first_speaker == speaker_1)
	{
		set_speach_timer(speaker_1,0.5)
		set_speach_timer(speaker_2,1)
	}
	else 
	{
		set_speach_timer(speaker_1,0.5)
		set_speach_timer(speaker_2,1)
	}
}

function set_speach_timer(speaker, seconds)
{
	speaker.alarm[3] = seconds * global.ROOM_SPEED_STANDARD;
}