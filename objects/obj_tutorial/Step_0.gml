/// @description cycle page on arrow keys


if(keyboard_check_pressed(vk_escape))
{
	return_to_title();
}
else if(keyboard_check_pressed(vk_anykey))
{
	next_page();
}