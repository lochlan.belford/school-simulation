/// @description Insert description here

// Inherit the parent event
event_inherited();

enum TUTORIAL_PAGES {
	OBJECTIVE,
	CONTROLS,
	MENU,
	CREATION,
	length
}

page_index = TUTORIAL_PAGES.OBJECTIVE


title = "How to Play"

function return_to_title()
{
	room_goto(rm_new_student_title);
}

//Give x and other button that both return to title
close_button = instance_create_layer(bbox_left + 8, bbox_top + 8, "buttons",abstract_button)
close_button.build_button_function(spr_pencil_x, return_to_title);
//return button
return_button = instance_create_layer(bbox_left + 96,bbox_bottom - 64,"buttons",abstract_button);
return_button.build_button_full(spr_button_small,"Title",return_to_title)


function next_page()
{
	if(page_index < TUTORIAL_PAGES.length - 1)
	{
		page_index += 1;
	}
	else
	{
		page_index = 0;
	}
	load_page(page_index);
}

function next()
{
	obj_tutorial.next_page()
}

function prev()
{
	obj_tutorial.next_page();
}

function load_page(page_index)
{
	layer_set_visible("diagrams_movement", false)
	layer_set_visible("diagrams_creation", false)
	layer_set_visible("diagrams_interact", false)
	layer_set_visible("diagrams_goal", false)
	switch(page_index){
		case TUTORIAL_PAGES.CONTROLS:
			load_controls();
			break;
		case TUTORIAL_PAGES.MENU:
			load_menu();
			break;
		case TUTORIAL_PAGES.CREATION:
			load_creation();
			break;
		case TUTORIAL_PAGES.OBJECTIVE:
		default:
			load_goal();
			break;
	}
}

function load_controls()
{
	layer_set_visible("diagrams_movement", true)
	layer_set_visible("diagrams_interact", true)
	title = "Movement and Controls"
	ds_list_set(lines,0,"Movement:")
	ds_list_set(lines,1,"Click anywhere in the environment and your character will move towards that spot.")
	ds_list_set(lines,2,"You may also move using the arrow keys or WASD.")
	ds_list_set(lines,3,"");
	ds_list_set(lines,4,"Interacting with the Environment:")
	ds_list_set(lines,5,"You can click on classrooms, lockers, or other students to interact with them.")
	ds_list_set(lines,6,"(Your character will automatically move toward whatever you select.)")
	ds_list_set(lines,7,"---- Classrooms: End class change and enter the classroom.")
	ds_list_set(lines,8,"---- Lockers: If the locker is yours, open it.")
	ds_list_set(lines,9,"---- Students: Introduce yourself and learn their name!")
	ds_list_set(lines,10,"");	
}

function load_goal()
{
	layer_set_visible("diagrams_goal", true)
	title = "How to Play"
	ds_list_set(lines,0,"Objective:")
	ds_list_set(lines,1,"You play as a student on their first day of class at a new school.")
	ds_list_set(lines,2,"Your objective is to learn the names of other students over the course of the day!")
	ds_list_set(lines,3,"You can learn a student's name by interacting with them in the hallway.")
	ds_list_set(lines,4,"You will also learn the names of a few students in each class you attend.");
	ds_list_set(lines,5,"Game Flow:")
	ds_list_set(lines,6,"The school day is comprised of four classes.")
	ds_list_set(lines,7,"There is a 2 minute class change before each.")
	ds_list_set(lines,8,"Watch the clock, and try to make it to class on time!")
	ds_list_set(lines,9,"After you've learned someone's name, you can see it above them in the hallway.")
	ds_list_set(lines,10,"At the end of the game, you will see a list of all the students you met!")
	
}

function load_menu()
{
	title = "The Menu"
	ds_list_set(lines,0,"Press Escape or click the menu button to view your menu")
	ds_list_set(lines,1,"The menu is comprised of four notecards:")
	ds_list_set(lines,2,"---- Schedule: Lists the order in which you should visit your four classes")
	ds_list_set(lines,3,"---- Locker: Lists your locker number")
	ds_list_set(lines,4,"---- People I've Met: A running tally of all the names you have learned")
	ds_list_set(lines,5,"---- How to Play: an abridged version of this tutorial")
	ds_list_set(lines,6,"You can use the mouse wheel or up/down to scroll through the name list")
	ds_list_set(lines,7,"You may switch between cards by clicking Next and Prev, or with left/right")
	ds_list_set(lines,8,"Click the return button or press escape again to return to the game")
	ds_list_set(lines,9,"You may also click Exit Game in the upper-left to return to the title screen.")
	ds_list_set(lines,10,"")
}

function load_creation()
{
	layer_set_visible("diagrams_creation", true)
	title = "Character Creation"
	ds_list_set(lines,0,"At the start of the game, you create the student you will play as.")
	ds_list_set(lines,1,"Each student is a geometric shape with a unique size and color.")
	ds_list_set(lines,2,"")
	ds_list_set(lines,3,"")
	ds_list_set(lines,4,"Use the buttons in the lower-left to select your base shape.")
	ds_list_set(lines,5,"Then, use the sliders to customize your appearance")
	ds_list_set(lines,6,"The first two sliders control your width and height")
	ds_list_set(lines,7,"The other three create a color using hue, saturation, and luminosity")
	ds_list_set(lines,8,"You may also give your student a name by typing in the name field")
	ds_list_set(lines,9,"When you are ready, click begin in the lower right!")
	ds_list_set(lines,10,"")
}

//next button
next_button = instance_create_layer(bbox_right - 96,bbox_top + 64,"buttons",abstract_button);
next_button.build_button_full(spr_button_small,"Next ->",next)


load_page(page_index)