/// @description Insert description here
// You can write your code in this editor

//Pause and Unpause code
if(global.paused)
{
	//Stop moving (only do this once)
	if(not am_paused)
	{
		am_paused = true;
		old_speed = speed;
		speed = 0;
	}
	//Cycle all alarms (do this every step)
	for(var a = 0; a < 4; a++)
	{
		alarm[a] += 1;
	}
}

if(am_paused and !global.paused)
{
	am_paused = false;
	speed = old_speed
}


move_wrap(true, false, sprite_width);

//if reached destination and enough time has passed, despawn
if(time_to_leave and reached_destination(x, y, destination_x, destination_y, destination_epsilon))
{
	instance_destroy();
}

//if speak
if(speak)
{
	draw_bubble = true;
	alarm[2] = 1 * room_speed;
	speak = false;
}

//Check for collision with upper and lower bounds of hallway
if(y < upper_bound) //This is confusing because y increases downward
{
	//Choose downward direction
	direction = random_range(180, 360)
}
else if(y > lower_bound)
{
	//choose upward direction
	direction = random_range(0, 180);
}