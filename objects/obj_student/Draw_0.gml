/// @description Insert description here
// You can write your code in this editor
draw_sprite_ext(sprite_index, sprite_choice, x, y, image_xscale, image_yscale, image_angle, blend_color, image_alpha);

//if set to speak, spawn a text bubble
if(draw_bubble){
	draw_sprite(spr_bubble_talk, 0, x + (sprite_width / 2) + 10, y - (sprite_height));
	//Also draw the selected glyph within that bubble
	draw_sprite_ext(spr_glyphs, talk_glyph, x + (sprite_width / 2) + 42, y - (sprite_height) - 40, 2, 2,0,c_white,1);
}

//Draw highlight outline on top if selected
if(selected){
	draw_sprite_ext(spr_student_highlight, sprite_choice, x, y, image_xscale, image_yscale, image_angle, c_white, image_alpha);
}
//draw name (DEBUG: id number) above self
if(debug_mode){
	draw_text_color(x - (sprite_width / 2), y - (sprite_height) - 120, name, c_purple, c_purple, c_purple, c_purple, 1);
	draw_text_color(x - (sprite_width / 2), y - (sprite_height) - 80, student_id, c_purple, c_purple, c_purple, c_purple, 1);
	draw_text(x - (sprite_width / 2), y - (sprite_height) - 40, global.enum_movement_states[move_state]);
}
else{
	draw_text_color(x - (sprite_width / 2), y - (sprite_height) - 40, name, c_purple, c_purple, c_purple, c_purple, 1);
}

