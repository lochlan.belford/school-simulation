/// @description Insert description here
// You can write your code in this editor

//if late_for_class, always pick TO_CLASS
if(global.late_for_class)
{
	move_state = MOVEMENT_STATES.TO_CLASSROOM;
}
//otherwise, choose a movement state randomly
else
{
	move_state = irandom_range(1, MOVEMENT_STATES.length - 1);
}	

//set parameters for that state
switch(move_state){
	case MOVEMENT_STATES.STOP:
		speed = 0;
		debug_move_state_text = "STOP";
		break;
	case MOVEMENT_STATES.WANDER:
		speed = 3;
		direction = random_range(0, 360);
		debug_move_state_text = "WANDER";
		break;
	case MOVEMENT_STATES.WALK:
		speed = 5;
		direction = random_range(0, 360);
		debug_move_state_text = "WALK";
		break;
	case MOVEMENT_STATES.RUN:
		speed = 9;
		direction = random_range(0, 360);
		debug_move_state_text = "RUN";
		break;
	case MOVEMENT_STATES.TO_CLASSROOM:
		speed = 3;
		direction = point_direction(x, y, destination_x, destination_y);
		debug_move_state_text = "TO_CLASS";
		break;
	case MOVEMENT_STATES.TO_SPOTLIGHT:
		debug_move_state_text = "TO_SPOT";
	default:
		speed = 1;
		direction = random_range(0, 360);
		break;
}

//Reset alarm for next change
alarm[3] = irandom_range(MOVE_CHANGE_FAST, MOVE_CHANGE_SLOW);