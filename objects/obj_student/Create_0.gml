/// @description Insert description here
// You can write your code in this editor

//DEBUG: Temp variables while removing logic from here
student_id = "N/A";
blend_color = c_white;
my_info = {};
talk_glyph = 0;
name = "NOT SET";
selected = false;
move_state = MOVEMENT_STATES.START;
x_scale = 1;
y_scale = 1;
am_paused = false;

//Each Student randomly assigned one of 3 basic sprites on creation
sprite_index = spr_students;
sprite_choice = irandom(2);

//DEBUG: Student randomly assigned favorite subject and an interest
interest = irandom(INTERESTS.length - 1);
fav_subject = irandom(SUBJECTS.length - 1);

speed = random_range(1, 10);
direction = random_range(180, 360);
//DEBUG: First try at defining limits of hallway
upper_bound = y
lower_bound = room_height - 50;

//DEBUG first try destination code
destination_number = irandom_range(101,104)
destination_x = 0; 
destination_y = 0;
destination_epsilon = 5;
destination_classroom = undefined;
for(var i = 0; i < instance_number(obj_classroom); i++){
	var classroom = instance_find(obj_classroom, i);
	if classroom.room_number == destination_number {
		destination_x = classroom.x;
		destination_y = classroom.y;
		destination_classroom = classroom;
		break;
	}
}

time_to_leave = false;
//Create alarm to activate leave flag after 60 seconds
alarm[0] = 60 * room_speed;

//Create alarm to check for activating "speech" once a second
speak = false;
draw_bubble = false;
alarm[1] = room_speed;

//Create alarm for mixing up movement every lower-upper seconds
MOVE_CHANGE_FAST = 3 * room_speed
MOVE_CHANGE_SLOW = 6 * room_speed
alarm[3] = irandom_range(MOVE_CHANGE_FAST, MOVE_CHANGE_SLOW);

debug_move_state_text = "START";

student_info = undefined;

/* unpack_info()
	pre-condition: student_info must be defined as a relevant Student struct
	this function takes each field of student_info and applies it to the instance variables.
	This should set the following:
		Name, blend_color, x and y scales
		Class schedule and destination 
		interest and subject favorability lists
		relationship map
		
	DEBUG: or maybe just appearance
*/
function unpack_info()
{
	if(student_info != undefined)
	{
		//set id, name, shape, height and width, and tint
		student_id = student_info.id;
		name = student_info.name;
		sprite_choice = student_info.sprite;
		//This sprite is then tansformed and tinted randomly
		y_scale = student_info.yscale;
		x_scale = student_info.xscale;
		blend_color = make_color_hsv(student_info.hue, student_info.sat, student_info.lum);
	}
}