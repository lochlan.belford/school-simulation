/// @description Insert description here

// Inherit the parent event
event_inherited();

centered = true;
title = "The New Student"
ds_list_set(lines,0,"A game about making friends!");
ds_list_set(lines,9,"Created by Lochlan Belford");
ds_list_set(lines,10, "Made in Game Maker Studio 2");

how_to_play_button = instance_create_layer(room_width/2, y + line_height*3,"buttons",abstract_button)
start_button = instance_create_layer(room_width/2, y + line_height*5,"buttons",abstract_button)

function start_game()
{
	room_goto(rm_create_char);
}
function how_to_play()
{
	room_goto(rm_tutorial);
}

how_to_play_button.build_button_full(spr_button_med, "How to Play", how_to_play);
start_button.build_button_full(spr_button_med, "Start Game",start_game);