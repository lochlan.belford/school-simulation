/// @description Setup Class text


// Inherit the parent event
event_inherited();

global.paused = true;
global.interaction_in_progress = true;

x = camera_get_view_x(view_camera[0]) + global.PAPER_X;
y = camera_get_view_y(view_camera[0]) + global.PAPER_Y;

//Class summary always involves at least two buttons
buttons = ds_list_create();


title = "Your First Day of " + obj_school_day.player_classroom.room_title;

//create a panel for displaying names
name_panel = instance_create_layer(x,y+line_height * 5,"animations",obj_name_panel);

function display_met_students(met_students)
{
	name_panel.build_panel(sprite_width - sprite_xoffset,3*line_height,met_students);
	ds_list_set(lines,3,"you have learned " + string(ds_list_size(name_panel.names)) + " more students' names!");
}

//test lines
ds_list_set(lines,1,"You have a good first day of " + obj_school_day.player_classroom.room_title + " class.");

ds_list_set(lines,2,"Between roll call and group activities, by the end of class");
ds_list_set(lines,3,"you have learned " + string(ds_list_size(name_panel.names)) + " more students' names!");


//Create an x in upper left to close every interaction
close_button = instance_create_layer(bbox_left + 8, bbox_top + 8, "interactions",abstract_button)
close_button.build_button(spr_pencil_x);
ds_list_add(buttons, close_button);

//create a continue button in the lower center (same effect)
continue_button = instance_create_layer(camera_get_view_x(view_camera[0]) + global.VIEWPORT_WIDTH/2, y + line_height*9, "interactions",abstract_button)
continue_button.build_button_text(spr_button_med,"End Class");
ds_list_add(buttons, continue_button);

