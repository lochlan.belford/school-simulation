/// @description Clean up and return to main game

//destroy name panel
instance_destroy(name_panel);

//Destroy all buttons
for(var b = 0; b < ds_list_size(buttons); b++)
{
	var button = ds_list_find_value(buttons,b)
	if(instance_exists(button))
	{
		with(button)
		{
			instance_destroy();
		}
	}
}
//clean up data structures
ds_list_destroy(buttons);

//unpause game
global.interaction_in_progress = false;
global.paused = false;

//Call begin_class change with obj_school_day
obj_school_day.begin_next_class_change();

//Goodbye! (parent destroy)
event_inherited();

