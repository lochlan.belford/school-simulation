/// @description Setup default NPC behavior

event_inherited();

known_to_player = false;

time_to_leave = false;

//Create alarm to activate leave flag after 60 seconds
alarm[0] = global.CLASS_CHANGE_LENGTH/3 * global.ROOM_SPEED_STANDARD;

//Create alarm to check for activating "speech" once a second
speak = false;
draw_bubble = false;
alarm[1] = global.ROOM_SPEED_STANDARD;

//Create alarm for mixing up movement every lower-upper seconds
MOVE_CHANGE_FAST = 3 * global.ROOM_SPEED_STANDARD
MOVE_CHANGE_SLOW = 6 * global.ROOM_SPEED_STANDARD
alarm[3] = irandom_range(MOVE_CHANGE_FAST, MOVE_CHANGE_SLOW);

//Ensures direction is downward
function spawn_from_classroom()
{
	enter_random_movement_state();
	direction = random_range(180,360)
}

//ensures direction is rightward
function spawn_from_outside()
{
	enter_movement_state(MOVEMENT_STATES.WALK);
	direction = random_range(-90,90);
}

//Player interaction method
function player_interaction()
{
	show_debug_message("How Do You Do, Fellow Kid?")
	first_meeting = meet_student(my_info)
	known_to_player = known(my_info)
	if(first_meeting)
	{
		//TODO: If we have already met, add a new, different line of data
		var interaction = instance_create_layer(x,y,"interactions",obj_interaction_student);
		//Interaction successfully spawned
		if instance_exists(interaction)
		{
			return true;
		}
	}
	//if no conversation, return false
	return false;
}

//officially record any changes to my_info, so that it is correctly passed to next location
function package_info()
{
	my_info.classes_visited += 1;
	ds_map_copy(my_info.relationships, relationships);
	my_info.known_to_player = known_to_player;
	return my_info;
}