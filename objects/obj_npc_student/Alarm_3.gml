/// @description Change Movement state

//if late_for_class, always pick TO_CLASS
if(global.late_for_class)
{
	enter_movement_state(MOVEMENT_STATES.TO_CLASSROOM);
}
//otherwise, choose a movement state randomly
else
{
	enter_random_movement_state();
}

//Reset alarm for next change
alarm[3] = irandom_range(MOVE_CHANGE_FAST, MOVE_CHANGE_SLOW);