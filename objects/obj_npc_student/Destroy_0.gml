/// @description Insert description here
// You can write your code in this editor

//Students are destroyed only when reaching destination classroom, so pass Student struct over to that room
var student_info = package_info()
with(destination_classroom){
	ds_list_add(arriving_students, student_info);
	num_new_students = ds_list_size(arriving_students);
}
// Inherit the parent event at the END to complete garbage collection
event_inherited();

