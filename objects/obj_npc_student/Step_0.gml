/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

//if reached destination and enough time has passed, despawn
if(time_to_leave and reached_destination(x, y, destination_x, destination_y, destination_epsilon))
{
	instance_destroy();
}

//if speak
if(speak)
{
	draw_bubble = true;
	alarm[2] = 1 * global.ROOM_SPEED_STANDARD;
	speak = false;
}