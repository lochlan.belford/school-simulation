/// @description Speech roll

//10% chance of spawning a speech bubble
speech_roll = random(100);
if (speech_roll < 10.0){
	self.speak = true;
	talk_glyph = irandom(9);
}
else
{
	var stagger = irandom_range(-10,10);
	alarm[1] = global.ROOM_SPEED_STANDARD + stagger;
}
