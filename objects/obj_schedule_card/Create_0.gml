/// @description Creates schedule text
// You can write your code in this editor

event_inherited();

title = "My Class Schedule"
show_line_numbers = true;
var class_number_to_title = ds_map_create()
ds_map_add(class_number_to_title,101,"Math, Room 101")
ds_map_add(class_number_to_title,102,"Social Studies, Room 102")
ds_map_add(class_number_to_title,103,"English, Room 103")
ds_map_add(class_number_to_title,104,"Science, Room 104")
var player_schedule = global.player_data.schedule;
//fill lines of card based on schedule
for(var c = 0; c < ds_list_size(player_schedule); c++)
{
	var next_line = ds_map_find_value(class_number_to_title,ds_list_find_value(player_schedule,c));
	ds_list_set(lines,c,next_line);
}
//Then delete interim data structure
ds_map_destroy(class_number_to_title);