/// @description Insert description here
// You can write your code in this editor
draw_self();
draw_set_font(fnt_locker);
draw_set_halign(fa_left);
draw_set_valign(fa_top);

//Only draw numbers on closed lockers
if(image_index == LOCKER_STATES.BOTH_CLOSED or image_index == LOCKER_STATES.TOP_OPEN)
{
	draw_text_color(locker_num_x, lower_locker_num_y, number_string_lower,c_black,c_black,c_black,c_black,1);
}
if(image_index == LOCKER_STATES.BOTH_CLOSED or image_index == LOCKER_STATES.BOTTOM_OPEN)
{
	draw_text_color(locker_num_x, upper_locker_num_y,number_string_upper,c_black,c_black,c_black,c_black,1);
}

//if set to highlight or select, draw appropriate highlight sprite
if(select_bottom)
{
	draw_sprite(spr_locker_highlight,1,x,y)
}
else if(highlight_bottom)
{
	draw_sprite(spr_locker_highlight,0,x,y)
}
if(select_top)
{
	draw_sprite(spr_locker_highlight,1,x,y-128)
}
else if(highlight_top)
{
	draw_sprite(spr_locker_highlight,0,x,y-128)
}