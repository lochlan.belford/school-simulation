/// @description Insert description here
// You can write your code in this editor

//spawn a highlight over each locker
bottom_highlight = instance_create_layer(x,y,"lockers",obj_locker_highlight)
top_highlight = instance_create_layer(x,y-128,"lockers",obj_locker_highlight)

function set_locker_number(num){
	locker_number = num
	number_string_upper = locker_number;
	number_string_lower = locker_number + 1;
	//add leading zeros to 1 and 2 digit numbers
	if(number_string_upper < 100)
	{
	
		if(number_string_upper < 10)
		{
			number_string_upper = "00" + string(number_string_upper);
		}
		else
		{
			number_string_upper = "0" + string(number_string_upper);
		}
	}
	if(number_string_lower < 100)
	{
	
		if(number_string_lower < 10)
		{
			number_string_lower = "00" + string(number_string_lower);
		}
		else
		{
			number_string_lower = "0" + string(number_string_lower);
		}
	}
}

set_locker_number(get_next_locker_number())


locker_num_x = x + 21
upper_locker_num_y = y - sprite_height + 16
lower_locker_num_y = upper_locker_num_y + 126

highlight_top = false;
highlight_bottom = false;
select_top = false;
select_bottom = false;

//DEBUG: Set lockers to highlight randomly as if haunted
//alarm[0] = room_speed * 5

//Open or close top locker (change image_index appropriately
function interact_locker(number){
	//Odd number means trying to interact with top locker
	if(number == locker_number)
	{
		switch(image_index)
		{
			//These two cases involve opening the top locker, advance image_index by one
			case LOCKER_STATES.BOTH_CLOSED:
			case LOCKER_STATES.BOTTOM_OPEN:
				image_index += 1;
				break;
			//The other two cases mean close the locker, decrement image index
			case LOCKER_STATES.TOP_OPEN:
			case LOCKER_STATES.BOTH_OPEN:
				image_index -= 1;
				break;
			//If the image index is somehow something else, reset to closed
			default:
				image_index = LOCKER_STATES.BOTH_CLOSED;
				break;
		}
		//tell method caller that interaction was successful
		return true;
	}
	//Even number means attempting to interact with bottom locker
	else if(number == locker_number + 1)
	{
		switch(image_index)
		{
			//These two cases involve clsoing the bottom locker, decrease image_index by two
			case LOCKER_STATES.BOTH_OPEN:
			case LOCKER_STATES.BOTTOM_OPEN:
				image_index -= 2;
				break;
			//The other two cases mean close the locker, advance two frames
			case LOCKER_STATES.TOP_OPEN:
			case LOCKER_STATES.BOTH_CLOSED:
				image_index += 2;
				break;
			//If the image index is somehow something else, reset to closed
			default:
				image_index = LOCKER_STATES.BOTH_CLOSED;
				break;
		}
		//tell method caller that interaction was successful
		return true;
	}
	//tell method caller that interaction failed
	return false;
}

//player interaction method
function player_interaction()
{
	show_debug_message("I'm a locker!")
	var right_locker = interact_locker(obj_player_student.locker_number);
	if(right_locker)
	{
		var interaction = instance_create_layer(x,y,"interactions",obj_interaction_locker);
		//Interaction successfully spawned
		if instance_exists(interaction)
		{
			return true;
		}
	}
	return false;
}