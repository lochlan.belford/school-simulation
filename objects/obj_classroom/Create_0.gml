/// @description Insert description here
// You can write your code in this editor

room_number = get_next_room_number();
room_title = ""

switch(room_number)
{
	case 101:
		room_title = "Math"
		break;
	case 102:
		room_title = "Social Studies"
		break;
	case 103:
		room_title = "English"
		break;
	case 104:
		room_title = "Science"
		break;
	default:
		room_title = "Elective"
}

students_spawned = 0
started_spawning = false;
continue_spawning = false;

students = ds_list_create();
num_old_students = 0;
arriving_students = ds_list_create();
num_new_students = 0

SECONDS_TO_SPWAN_ALL = 40;

//interaction variables
selected = false;

function start_class()
{
	show_debug_message("REACHED START CLASS")
	ds_list_copy(students, arriving_students);
	students_spawned = 0;
	started_spawning = false;
	continue_spawning = false;
	num_old_students = ds_list_size(students);
	ds_list_destroy(arriving_students);
	arriving_students = ds_list_create();
	num_new_students = 0;
	ds_list_shuffle(students);
}

//returns 1 if student spawned, 0 otherwise (for overall count)
function spawn_student()
{
	if(students_spawned < ds_list_size(students))
	{
		var student_info = ds_list_find_value(students,students_spawned)
		var student = instance_create_layer(x, y, "students", obj_npc_student)
		//finish initializing student
		with(student)
		{
			init_from_info(student_info);
			spawn_from_classroom();
		}
		students_spawned += 1;
		return 1;
	}
	return 0;
}

//player interaction method
function player_interaction()
{
	show_debug_message("I'm a classroom!")
	var interaction = instance_create_layer(x,y,"interactions",obj_interaction_classroom);
	//Interaction successfully spawned
	if instance_exists(interaction)
	{
		return true;
	}
}