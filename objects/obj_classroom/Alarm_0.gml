/// @description Insert description here
// You can write your code in this editor

//Spawn a student if any are left
var origin_room = room_number;
if(students_spawned < num_old_students)
{
	leaving_student = instance_create_layer(x, y, "students", obj_student);
	var student_info = ds_list_find_value(students, students_spawned);
	//alter student appearnace to match stored creation info
	with(leaving_student){
		//TODO: STUDENT STRUCT HAS CHANGED
		//Give them the whole Student struct to pass back later
		my_info = student_info;
		//Use individual values to immediately set appearance
		student_id = student_info.id;
		name = student_info.name;
		sprite_choice = student_info.sprite;
		//This sprite is then tansformed and tinted randomly
		y_scale = student_info.yscale;
		x_scale = student_info.xscale;
		blend_color = make_color_hsv(student_info.hue, student_info.sat, student_info.lum);
		
		//Now give them info about the class their leaving
		origin_class = origin_room;
		
		//set their subject, interest,
		interest = student_info.interest;
		fav_subject = student_info.fav_subject;
		//set best friend--DEBUG: this seems like the wrong way to do this
		manager = student_info.manager;
		//DEBUG: Populate a list of my relationships with all other students
		my_relationships = ds_list_create();
		best_friend_id = 0;
		var best_friend_val = -1;
		for(var s = 0; s < manager.STD_NUM_STUDENTS; s++)
		{
			var next_rel_val = irandom(9);
			ds_list_add(my_relationships, irandom(9));
			if(next_rel_val > best_friend_val)
			{
				best_friend_id = s;
				best_friend_val = next_rel_val;
			}	
		}
		best_friend_info = ds_list_find_value(manager.all_students, best_friend_id);
		
		//Get destination based on schedule
		destination_number = ds_list_find_value(student_info.schedule, student_info.classes_visited % 4);
		destination_x = 0; 
		destination_y = 0;
		destination_epsilon = 5;
		destination_classroom = undefined;
		for(var i = 0; i < instance_number(obj_classroom); i++){
			var classroom = instance_find(obj_classroom, i);
			if classroom.room_number == destination_number {
				destination_x = classroom.x;
				destination_y = classroom.y;
				destination_classroom = classroom;
				break;
			}
			
		//Trying setting built in scaling fields to change bounding boxes
		image_xscale = x_scale;
		image_yscale = y_scale;
		
}
	}
	//record spawned student
	students_spawned += 1;
	//set alarm to go again
	alarm[0] = spawn_rate;
}
