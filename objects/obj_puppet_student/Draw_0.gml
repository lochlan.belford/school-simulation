/// @description Insert description here

// Inherit the parent event
event_inherited();

//if set to speak, spawn a text bubble
if(draw_bubble){
	draw_sprite(spr_bubble_talk, 0, x + (sprite_width / 2) + 10, y - (sprite_height));
	//Also draw the selected glyph within that bubble
	draw_sprite_ext(spr_glyphs, talk_glyph, x + (sprite_width / 2) + 42, y - (sprite_height) - 40, 2, 2,0,c_white,1);
}

draw_text_color(x - (string_width(name) / 2), y - (sprite_height) - 40, name, c_purple, c_purple, c_purple, c_purple, 1);