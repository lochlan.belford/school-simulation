# School Simulation

A Game Maker 2 experiment focused on spawning many similar instances (students) and orchestrating their movements and interactions in efficient ways.

0.1.0 12/17/2020
* Created an abstract_student class that is the parent of npc_student and player_student
* Spawns player character! With all correct customized elemetns
* enabled keyboard movement
* created a view that follows the player

Next Up:
* Allow zooming of viewport
* hide names, discover on click
* user interface (pause, speed, talk)
* click to move
* press to talk
* Rework classrooms to work with new student objects
* state machine of day
* allow player to enter and leave classrooms
* notebook paper pop ups
* new art (clock, hallway tiles? lockers?)
* make doorways bigger?
* sound?
* tutorializing
* spotlight interactions (fade out layer behind)
* check who you have met (paper? phone?)
* Refactor buttons to be shared object

0.1.0 in progress 12/16/2020
Done today
* Created title screen
* game prgresses from title -> character creation -> hallway
* game saves character creation data
* new logic object to control flow of school day
* school day object spawns all students at left of screen at start of day
* Cleaned up creation of objects a little

Still to do for 0.1.0
* Spawn player character with correct customizations
* Player object with movement controls
* Create viewport that follows player
* Write logic for one complete school day (arrive, 4 classes, leave)

0.1.0 in progress 12/7/2020
To Do Today
* Rearrange Creation Screen
* Transition from creation room to hallway room
* Modify spawning to represent a whole school day (Enter from outside, go to four classes, leave school)
* Spawn player character and allow movement

0.0.12 12/3/2020
Accomplished
* Got back to work after a long period of painful inertia
* Added Subjects above classroom doors
* Finally showed game to other people, which was a bit like ripping my soul out of my body
* Decided on a direction to take the project--play as one particular student, build a web of relationships

Features for 0.1 (tomorrow!) and beyond
* Create a character
* interface for deciding what to say and when to say it
* Move by click or WASD?
* Enter into conversation
* Concept of "conversation events"
* Display summary of what happens during class
* Many more

* I am deciding to defy my anxiety and pursue a goal that scares me!

0.0.11 10/10/2020
Accomplished
* Made a rough title screen
* Learned that I need to learn a lot more about display sizes, view ports, etc.
* Worked on a Saturday!

Note to Self
* Let go of 0.1 being so important

0.0.10 10/7/2020
Accomplished
* Added Pause Button
* Defines what pausing means for students, classrooms, and manager
* Students can still be selected by click during pause
* Investigated saving student roster, decided to save for later

Further Improvements
* Pause button highlight on hover, click
* Since this will be a browser game, might save/load students through server-side python?

Steps left for 0.1
* Make a title screen
* Have options to start with 160 or 1600 students
* Make start, play, quit, exit loop
* Clean up old code
* Comments!
* README

0.0.9 10/5/2020
Accomplished
* Fixed UI crash on focused student despawning
* Students generate a (random, for now) list of their relationship values with all other students
* Background image and student movement bounds now scale to arbitrary size of room
* Created a basic "late for class" system that makes all students enter TO_CLASS mode after 2 minutes since class change began
* Fixed bugs related to not clearning classrooms' lists of leaving and entering students
* Students have random schedules, which they follow through multiple class changes--no more going back to the same class you just left!
* Classroom spawn rate scales inversely with number of students, useful for testing whole school at once
* Observed that having 1600 students on screen now runs at 90-100 fps--need to test with limited specs
* Used profiler to see that draw_text_color accounts for 60% (!!!) of total step time--fps doubles when text-above-student code is commented out

Further Improvements
* Find way to test with throttled specs

Steps left for 0.1.0
* Create a pause system that stil lets player click on students
* Save and load roster of students
* Allow regeneration of roster as option
* Title screen and room switching
* Make a real README and rename this to devlog

0.0.8 10/1/2020
Accomplished
* Layed out student info panel ui (reinventing the CSS wheel as I go...)
* Panel displays correct sprite, name, origin room, destination room, interest, subject, and best friend
* Played around with enums a lot for defining Interests and Subjects
* Student structs assigned random interest, subject, and friend id on creation
* After creation of all students, iterates again and turns all friend ids into actual Student structs

Further Improvements
* My current implementation of finding best friends seems like it might be really inefficient in both time and space
* I should just go ahead and write code to read student roster from external file, since that is how I will be doing it once it is really up and running

0.0.7 9/30/2020
Accomplished
* Created glyphs to be used in coversation! First time using multiple frames of one sprite to organize
* Whenever students draw a talk bubble, they also center a random glyph inside it
* Generated a list of 1600 random names, assigns names to all generated students
* Names displayed above head--only thing displayed in play mode (vs. debug)
* Consolidated student sprites to one sheet following glyph example
* Created outline sprites to overlay on students when selected (so outline is not color blended with base sprite)
* Created a student_info object to handle code for clicking on students and displaying their info in a panel
* Students can be selected by click, deselected by clicking on nothing, clicking the same student again, or clicking another student (which then selects them)

Further Improvements
* Really fixated on reaching "0.1" today which just meant I was not appreciating all the progress I made :(
* Actually write info in new ui panel, tweak appearance

0.0.6 9/29/2020
Accomplished
* Students store their full information struct
* Students pass their struct onto destination classroom on destroy
* Classrooms keep list of all arriving students
* Classrooms display number of students inside above door
* Obj_manager checks for all students having left first classroom and arrived at another one
* At this time, obj_manager iterates through each classroom and resets student lists to arriving list
* Spawning flags are then reset to allow a second class change
* Takeaway: Students are now persistant accross multiple class changes!

Further Improvements:
* What other data should live in the Student struct?

Next Tasks:
* All from below, and
* Create Student info UI for display upon selection

0.0.5 9/28/2020
Accomplished:
* No repeated students
* Students can move in 2 dimensions, between defined upper and lower y bounds
* Created an enum of movement states which are used to define speed, direction, and destination
* For now, students randomly select a new state every 3-6 seconds
* Current movement state displayed above head in debug mode
* They all still eventually reach their classrooms thanks to TO_CLASSROOM move state
* Increased incedence of talk bubbles to 10% chance every second (from 1%)
* Created enums for student interests and attributes to be used later

Further Improvements:
* More intelligent state choosing that just random chance
* Make transition states between main states? (speeding up, brief pauses to change direction)
* Need to make Spotlights to follow up on TO_SPOTLIGHT state

Next Tasks:
* Random name list
* Classrooms store students
* Spotlights and focusing system
* Click on student to see info (and have take priority in interactions?)
* Stuff in talk bubbles
* Have 10 linked hallways (for whole school of 1600 students 40 students * 4 classrooms * 10 halls)

0.0.4 9/25/2020
Accomplished:
* Students spawn empty talk bubbles, 1% chance per second
* Students display an id number above them (later this will be their name instead)
* Classrooms are spawned programmatically by obj_manager, are given a list of students
* Appearance of students spawned by classrooms matches the creation details from the classroom list

Future Improvements:
* IMPORTANT: Students have to actually be given their own Student struct so they can pass it to destination classroom!
* Classrooms should be able to receive a list of entering students, and clear old list

Next Tasks:
* 2D Student movement, with alterations to speed and direction
* Reading and assigning random names from a large list

0.0.3 9/24/2020
Accomplished:
* Created a Student struct for cleanly passing data between objects (from manager to classroom to student and back)
* Investigated a number of data structures for storing full student roster, settled on list
* Also good research for relationship data structure
* Created a talk bubble sprite
* Created a new font
* Learned a lesson about the dangers of multi-branch merging (I think the takeaway is that it is possible, but I NEED to commit the .ypp file always. I think I'll avoid it for a while regardless)

Future Improvements/Tommorrow's Goal:
* Still plenty of outstanding to-dos on notes lower down
* Learn how to use git-revert to extend my git-combos by linking my git-vert-tricks into git-manuals

0.0.2 9/23/2020
Accomplished:
* Created sprites for hallway and classrooms
* Created test hallway with four classrooms
* Classrooms have room number defined by static variable, displayed above door
* Classrooms can spawn students, currently one every step until 400 per door, triggered/paused by spacebar
* Students spawn with a random speed (1-10) and direction (left or right) and start moving immediately
* Students have a random destination room, and they despawn when they get within 5 pixels of its origin
* This despawn can't happen in first 5 seconds after spawning (first alarm usage)
* Getting about 800 FPS with 10MB memory usage when all 1600 students are on screen. Nice!

Future Improvements:
* Let classrooms store what students are inside
* Let students move in whole space below classrooms
* Give studens more interesting movement patterns (change dir, speed, stand still, etc)
* Define new fonts (to use above classrooms for example)

Tomorrow's goal:
* Let students spawn talk bubbles
* Give students names
* Allow students to greet each other by name
* Define spotlight areas for students to pair off to
* Start modelling relationships!
* Maybe some art refinement

0.0.1 9/22/2020
Accomplished:
* Adds the three fundamental sprites with first attempt at "chalk outline" effect.
* Creates test room and an background object that programmatically fills background with a color gradiant and randomizes the random seed
* Creates student object, which programmatically alter their appearance on creation based on random values
* Spawns 10 example students to see progress!

Future improvements:
* Play around with bounds for random scaling, and look into normally distributing the randoms
* Get the background gradiant to slowly move through the color spectrum

Tomorrow's goal:
* Create basic "hallway" room
* Get them moving!

0.0.0 9/22/2020

Initial Objectives
--Explore and learn GM2 capabilities for the following:
1. Spawning instances programmatically
2. Assigning sprites to instances programmatically
3. Altering sprites programmatically
4. Creating new sprites programmatically out of primitive shapes
5. Managing hundreds of instances efficiently
6. Employing a relational database or other data management
7. Querying that data regularly during gameplay

Initial Concept
Simulation of a school halway where students briefly interact in pairs during a class change

Students:
1. Are represented by simple (and hopefully unique) transformations of a few fundamental sprites (square, circle, triangle)
2. Spawn at a classroom door, are assigned a new classroom to go to, mill around for 5 minutes, and then despawn at that assigned destination
3. Are able to identify other students in the crowd, pair off, and have an interaction based on the defined relationship of that pair
4. Students have unique ids

School Hallway:
1. A simple representation--line for floor, large rectangles for classroom doors, background color

Relationships:
1. Initial nutso idea for relationship modeling is a huge relational database that stores the relationship for every possible ordered pair of students (x, y). Order is essential to allow asymmetric relationships, including that most important of school relationships, the unrequited crush
2. Using this approach there are exactly x**2 - x rows in the relationship table for x students.
3. My crazy lynchpin idea is that relationships can be looked up in constant time by using the binary representations of two students' ids and appending them to one another to create 2 unique numbers. These are the two rows to fetch for the pair. It is possible that this is totally useless and using a normal ordered pair would be just as fast/slow, but it made me feel very smart!

Short-term goal:
Learn more about game maker by using it

Medium-term goal:
If this runs efficiently, it would be a cool thing to see running in the background of my portfolio website

Long-term goal:
Find a way to make a game out of this idea--perhaps the player chooses a student to be and attempts to play out a series of interactions with particular other students?