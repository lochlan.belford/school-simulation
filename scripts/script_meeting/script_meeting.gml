// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

//Checks if a given student record has already been recorded in student relationships
function known(student_info){
	//get my relationship map entry from player data, if exists
	var my_entry = ds_map_find_value(global.player_data.relationships,student_info.id)
	//if undefined, not known
	if(is_undefined(my_entry))
	{
		return false;
	}
	return true;
}

//returns true on first meeting
function meet_student(student_info)
{
	if(known(student_info))
	{
		return false;
	}
	else
	{
		my_entry = ds_map_create()
		ds_map_add(my_entry,"name",student_info.name)
		//add to player's overall map
		ds_map_add(global.player_data.relationships,student_info.id,my_entry);
		//also add my name to their name list
		ds_list_add(global.player_data.names_in_order,student_info.name);
		student_info.known_to_player = true;
		return true;
	}
}