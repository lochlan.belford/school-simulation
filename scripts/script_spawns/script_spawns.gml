// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function create_test_students(){
	show_debug_message("in create_test_students");
	//Get position for first instance
	var next_y = 1080 / 3;
	for(var r = 0; r < 2; r++){
		var next_x = 1920 / 5;
		for(var c = 0; c < 5; c++){
			instance_create_layer(next_x, next_y, "students", obj_student);
			next_x += 1920 / 10;
		}
		next_y += 1080 / 3;
	}
}

function get_next_room_number(){
	static next_room_number = 100;
	next_room_number += 1;
	return next_room_number;
}

function get_next_locker_number(){
	static next_locker_number = -1;
	next_locker_number += 2;
	return next_locker_number;
	
}

function get_next_board(){
	static next_board = 0;
	next_board += 1
	return next_board;
}