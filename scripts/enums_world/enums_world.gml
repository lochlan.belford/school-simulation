// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum GAME_STATES{
	START,
	CLASS_CHANGE,
	BELL_RUNG,
	CLASS_IN_SESSION,
	END
}

enum LOCKER_STATES{
	BOTH_CLOSED,
	TOP_OPEN,
	BOTTOM_OPEN,
	BOTH_OPEN
}

enum BUTTON_STATES{
	DEFAULT,
	HOVER,
	CLICK
}