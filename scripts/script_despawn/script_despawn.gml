// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function reached_destination(_traveller_x, _traveller_y, _dest_x, _dest_y, _epsilon){
	result = abs(_traveller_x - _dest_x) <= _epsilon and abs(_traveller_y - _dest_y) <= _epsilon;
	return result;
}