// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

//STOP is first, TO_CLASSROOM is last
global.enum_movement_states = ["Start", "Stop", "Wander", "Walk", "Run", "To Spot", "To Class"];
enum MOVEMENT_STATES {
	START,
	STOP,
	WANDER,
	WALK,
	RUN,
	TO_SPOTLIGHT,
	TO_CLASSROOM,
	length
}

enum ROUTINES {
	TO_LOCKER,
	TO_CLASS,
	CONGREGATE,
	CONVERSATION,
	RUN_AROUND,
	STAND,
	length
}

global.enum_interests = ["Science", "Art", "Theatre", "Film", "Math", "Reading", "The World", "TV", "Improv", "Video Games", "Board Games"];
enum INTERESTS {
	SCIENCE,
	ART,
	THEATRE,
	FILM,
	MATH,
	BOOKS,
	WORLD,
	TV,
	IMPROV,
	VIDEO_GAMES,
	BOARD_GAMES,
	length
}

global.enum_subjects = ["Science", "Math", "English", "History", "Civics", "Gym", "Band", "Chorus", "Languages", "Visual Art", "Theatre", "Shop"];
enum SUBJECTS {
	SCIENCE,
	MATH,
	ENGLISH,
	HISTORY,
	CIVICS,
	GYM,
	BAND,
	CHORUS,
	LANGUAGES,
	ART,
	THEATRE,
	SHOP,
	length
}

enum ATTRIBUTES {
	CHATTY,
	SHY,
	LATE,
	EARLY,
	length
}

