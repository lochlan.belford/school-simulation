{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 4,
  "gridY": 4,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"0a73ec2f-6f56-40cf-8125-cd407b741386","path":"sprites/spr_std_circ/spr_std_circ.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0a73ec2f-6f56-40cf-8125-cd407b741386","path":"sprites/spr_std_circ/spr_std_circ.yy",},"LayerId":{"name":"957a3efa-953d-4184-9c8b-e062716585e7","path":"sprites/spr_std_circ/spr_std_circ.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_std_circ","path":"sprites/spr_std_circ/spr_std_circ.yy",},"resourceVersion":"1.0","name":"0a73ec2f-6f56-40cf-8125-cd407b741386","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3437e567-6a88-499e-b2b9-0ed1d533555e","path":"sprites/spr_std_circ/spr_std_circ.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3437e567-6a88-499e-b2b9-0ed1d533555e","path":"sprites/spr_std_circ/spr_std_circ.yy",},"LayerId":{"name":"957a3efa-953d-4184-9c8b-e062716585e7","path":"sprites/spr_std_circ/spr_std_circ.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_std_circ","path":"sprites/spr_std_circ/spr_std_circ.yy",},"resourceVersion":"1.0","name":"3437e567-6a88-499e-b2b9-0ed1d533555e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_std_circ","path":"sprites/spr_std_circ/spr_std_circ.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d58b218f-6e5f-4c56-94f6-7582dd6eb33e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0a73ec2f-6f56-40cf-8125-cd407b741386","path":"sprites/spr_std_circ/spr_std_circ.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"45afe6b2-d803-43a3-b1b3-fa7b43729716","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3437e567-6a88-499e-b2b9-0ed1d533555e","path":"sprites/spr_std_circ/spr_std_circ.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 64,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_std_circ","path":"sprites/spr_std_circ/spr_std_circ.yy",},
    "resourceVersion": "1.3",
    "name": "spr_std_circ",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"957a3efa-953d-4184-9c8b-e062716585e7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "UserInterface",
    "path": "folders/Sprites/UserInterface.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_std_circ",
  "tags": [],
  "resourceType": "GMSprite",
}